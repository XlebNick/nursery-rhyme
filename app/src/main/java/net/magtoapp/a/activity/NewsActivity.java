package net.magtoapp.a.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import ru.magtoapp.xlebnick.a.R;

public class NewsActivity extends TemplateActivity implements SwipeRefreshLayout.OnRefreshListener {

    private WebView webView;
    private SwipeRefreshLayout swipeLayout;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_news);
        setWhileLineVisible(false);
        setExtraBackButtonVisible(true);
        setPaddingContent(0, 0, 0, 0);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeLayout.setColorSchemeColors(Color.rgb(76, 200, 226));
        swipeLayout.setOnRefreshListener(this);

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(getIntent().getExtras().getString("link"));
        webView.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.INVISIBLE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);
                if (progress == 100) {
                    swipeLayout.setRefreshing(false);
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
                    webView.setVisibility(View.VISIBLE);
                    showContent();
                }
            }

        });

    }

    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;


    @Override
    public void onStart() {
        super.onStart();

        swipeLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (webView.getScrollY() == 0)
                            swipeLayout.setEnabled(true);
                        else
                            swipeLayout.setEnabled(false);

                    }
                });
        findViewById(R.id.scroll_view).getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = findViewById(R.id.scroll_view).getScrollY();
                if(scrollY == 0) swipeLayout.setEnabled(true);
                else swipeLayout.setEnabled(false);

            }
        });
    }

    @Override
    public void onStop() {
        swipeLayout.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        webView.reload();
    }
}
