package net.magtoapp.a.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.listener.OnSeekCompletionListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.fragment.CommentsFragment;
import net.magtoapp.a.fragment.DetailsFragment;
import net.magtoapp.a.fragment.OtherVideosFragment;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.recyclervieweffects.FlingBehavior;
import net.magtoapp.a.server.CommentRequest;
import net.magtoapp.a.server.GeneralResponse;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.view.PlayerControllerBar;

import static android.view.View.GONE;
import static java.util.Calendar.HOUR_OF_DAY;

//import com.google.android.gms.games.Player;

public class PlayerActivity extends TemplateActivity implements ExoPlayer.Listener, DetailsFragment.OnVideoDataLoadedListener, OnPreparedListener, OnSeekCompletionListener {

    private VideoData thisVideoData = new VideoData();

    //views
//    private TextureView surfaceView;
    private FrameLayout videoContainer;
    private ProgressBar progressBar;
    private View addCommentContainer;
    private ViewPager viewPager;
    private View playButton;
    //player
    private PlayerControllerBar controls;
    private String idToComment;
    private View panel;
    private int lastPosition = 0;

//    private YoutubeExoPlayer player;
    //need in fullscreen
    private int videoHeight;
    private boolean isFullscreen = false;
    private int lastOrientation = Configuration.ORIENTATION_PORTRAIT;

    private boolean lastPlayerState = false;
    //need in drag
    private boolean wasMovement = false;

    private float me_y = 0;

    private boolean wasMarkAsViewed = false;
    private boolean isActivityLeft = false;


    //ads
//    private long lastTimeShown = 0;
//    private boolean wasAdShown = false;

    public MyApiEndpointInterface apiService;

    private OnCommentAddedListener onCommentAddedListener;
//    private OnVideoPlayedListener onVideoPlayedListener;
    private OtherVideosFragment otherFragment;


    EMVideoView emVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = retrofit.create(MyApiEndpointInterface.class);
        if (getIntent().getExtras() != null){
            thisVideoData.channel.id = getIntent().getExtras().getString("channelId");
            thisVideoData.url = getIntent().getExtras().getString("url");
            thisVideoData.channel.title = getIntent().getExtras().getString("channel");
            thisVideoData.channel.humbnails._60x60 = getIntent().getExtras().getString("channelLogo");
        }
        else {
            thisVideoData.channel.id = "ad56367b2734916a280c25c215ad32a0";
            thisVideoData.url = "http://www.youtube.com/watch?v=8sp3AaiTmM8";
        }
        if (thisVideoData.url != null) {
            thisVideoData.id = thisVideoData.url.substring(thisVideoData.url.indexOf('=') + 1);
        }
//
//        if (getResources().getBoolean(R.bool.use_ads)){
//
//            String appKey = getString(R.string.appodeal_key);
//            Appodeal.disableLocationPermissionCheck();
//            Appodeal.initialize(this, appKey, Appodeal.NON_SKIPPABLE_VIDEO );
//        }

        //setup template
        setMainContentView(R.layout.activity_player);

        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(System.currentTimeMillis());
        if (date.get(HOUR_OF_DAY) < 6 || date.get(HOUR_OF_DAY) >= 20){

            Glide.with(this).load(R.drawable.night_blurred_background)
                    .dontAnimate()
                    .into((ImageView) findViewById(R.id.background));
        }

        emVideoView = (EMVideoView)findViewById(R.id.video_view);
        emVideoView.setControls(null);



        setPaddingContent(0, 0, 0, 0);
        showToolbar(false);
//        surfaceView = (TextureView) findViewById(R.id.surface_view);

        findViewById(R.id.back_btn_when_toolbar_hidden).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        videoContainer = (FrameLayout) findViewById(R.id.video_container);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        
        
        playButton = findViewById(R.id.play_button);
        panel = findViewById(R.id.panel);

        for (String string : menuItems){

            if (string.equalsIgnoreCase("search")){
                menuItems.remove(string);
                ((ArrayAdapter) navDrawerItems.getAdapter()).notifyDataSetChanged();
                break;
            }
        }
        viewPager.setAdapter(new UnderVideoPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0){

                    ((ViewGroup) findViewById(R.id.navig_details)).getChildAt(1).setAlpha(1- positionOffset);
                    ((ViewGroup) findViewById(R.id.navig_other_videos)).getChildAt(1).setAlpha(positionOffset);

                    ((ViewGroup) findViewById(R.id.navig_comments)).getChildAt(1).setAlpha(0);
                    AppBarLayout.LayoutParams params =
                            (AppBarLayout.LayoutParams) findViewById(R.id.appbar_top).getLayoutParams();
                    params.setScrollFlags( AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                    findViewById(R.id.appbar_top).setLayoutParams(params);

                } else if (position == 1){

                    ((ViewGroup) findViewById(R.id.navig_other_videos)).getChildAt(1).setAlpha(1 - positionOffset);
                    ((ViewGroup) findViewById(R.id.navig_comments)).getChildAt(1).setAlpha(positionOffset);

                    AppBarLayout.LayoutParams params =
                            (AppBarLayout.LayoutParams) findViewById(R.id.appbar_top).getLayoutParams();
                    params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL);
                    findViewById(R.id.appbar_top).setLayoutParams(params);

                } else if (position == 2){

                    ((ViewGroup) findViewById(R.id.navig_comments)).getChildAt(1).setAlpha(1 - positionOffset);
                    ((ViewGroup) findViewById(R.id.navig_details)).getChildAt(1).setAlpha(0);

                    AppBarLayout.LayoutParams params =
                            (AppBarLayout.LayoutParams) findViewById(R.id.appbar_top).getLayoutParams();
                    params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                    findViewById(R.id.appbar_top).setLayoutParams(params);

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) findViewById(R.id.appbar).getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });

        addCommentContainer =  findViewById(R.id.addcomment_container);

        //controlbar for video
        controls = (PlayerControllerBar) findViewById(R.id.controls);
       
        videoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (controls.isShowing()){
                    controls.hide();
                } else {
                    if (emVideoView != null && emVideoView.isPlaying()){
                        controls.show(3);

                    }
                }
            }
        });
//        surfaceView.requestFocus();

        //find video height
        final Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        videoHeight = (int) ((size.x * 9.0f) / 16.0f);

        emVideoView.setMeasureBasedOnAspectRatioEnabled(true);
//        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) surfaceView.getLayoutParams();
//        lp.height = videoHeight;
//        surfaceView.setLayoutParams(lp);

        controls.setMediaPlayer(emVideoView);
        controls.setPlayWhenReadyListener(this);


    }


    private void hideAddContainerView(){
        addCommentContainer.setVisibility(GONE);
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        view.clearFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (addCommentContainer.getVisibility() == View.VISIBLE){
            hideAddContainerView();
        } else {
            Intent resultIntent = new Intent();
            ArrayList<String> visitedVideos = new ArrayList<>();
            if (wasMarkAsViewed)
                visitedVideos.add(thisVideoData.id);
            resultIntent.putExtra(VideoAdapter.KEY_VISITED_NEWS, visitedVideos);
            setResult(RESULT_OK, resultIntent);
            super.onBackPressed();
        }
    }

    private void markAsViewed(){
        //set video as viewed
        final Call<String> setAsViewed;
        setAsViewed = apiService.setAsViewed(thisVideoData.id, Utils.getToken(this));
        setAsViewed.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
            }

        });

        if (!wasMarkAsViewed){
            wasMarkAsViewed = true;
            thisVideoData.views++;
            ((TextView) findViewById(R.id.panel_views)).setText(String.format(Locale.getDefault(), "%d", thisVideoData.views));
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        Glide.with(this)
                .load(thisVideoData.channel.humbnails._60x60)
                .into((ImageView) findViewById(R.id.channel_logo));
//        if (!wasAdShown){
////            AdToApp.showInterstitialAd();
//        }


    }

    @Override
    protected void onStop() {
//        AdToApp.onPause(this);
        controls.hide();
        if (emVideoView != null){
            stopPlayVideo();
//            emVideoView.stopPlayback();
            playButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        }


        startResizeAnimation(videoContainer, false, true);

        super.onStop();
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (System.currentTimeMillis() - Utils.getLastTimeAdShown(this) > 1000 * 60 * 60 && getResources().getBoolean(R.bool.use_ads)){
//            Appodeal.show(this, Appodeal.NON_SKIPPABLE_VIDEO);
//            Utils.setLastTimeAdShown(this, System.currentTimeMillis());
//        }
//    }

    @Override
    public boolean loadData() {
        if (getIntent().getExtras() != null){
            thisVideoData.channel.id = getIntent().getExtras().getString("channelId");
            thisVideoData.url = getIntent().getExtras().getString("url");
            thisVideoData.channel.title = getIntent().getExtras().getString("channel");
            findViewById(R.id.channel_logo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PlayerActivity.this, ChannelActivity.class);
                    intent.putExtra("id", thisVideoData.channel.id);
                    intent.putExtra("title", thisVideoData.channel.title);
                    intent.putExtra("description", "Description is not provided");
                    startActivity(intent);
                }
            });
        }
        else {
            thisVideoData.channel.id = "ad56367b2734916a280c25c215ad32a0";
            thisVideoData.url = "http://www.youtube.com/watch?v=8sp3AaiTmM8";
        }
        if (thisVideoData.url != null) {
            thisVideoData.id = thisVideoData.url.substring(thisVideoData.url.indexOf('=') + 1);
        }

        showContent();
        return true;
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, final int playbackState) {

        if (isActivityInFront){
            if (playWhenReady){
                controls.show(3);
                panel.setVisibility(GONE);
                playButton.setVisibility(GONE);
            } else {
                controls.hide();
                panel.setVisibility(View.VISIBLE);
                playButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {
        if (emVideoView == null)
            return;
        lastPlayerState = emVideoView.isPlaying();
        Log.d("lastPlayerState", lastPlayerState + "");
        if (lastOrientation == Configuration.ORIENTATION_LANDSCAPE){
            return;
        }

        if ( lastPlayerState ){
            startResizeAnimation(videoContainer, true, false);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) findViewById(R.id.appbar).getLayoutParams();
            params.setBehavior(null);
        } else {
            startResizeAnimation(videoContainer, false, false);

        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.lastOrientation = newConfig.orientation;
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {


            final Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int width = (int) ((size.x * 9.0f) / 16.0f);

            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT);
            lp.gravity = Gravity.TOP;
            lp.gravity = Gravity.CENTER;

            lp.height = width;
            emVideoView.setLayoutParams(lp);
            emVideoView.requestLayout();

        } else {
//
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) emVideoView.getLayoutParams();
            lp.height = videoHeight;
            lp.gravity = Gravity.CENTER;
            emVideoView.setLayoutParams(lp);
            if (!lastPlayerState){
                startResizeAnimation(videoContainer, false, false);
            }
        }
    }

    public void startResizeAnimation(final View viewToResize, final boolean isExpand, boolean isForce){
        if (isExpand){
            isFullscreen = true;
            hideSystemUI();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        } else {
            isFullscreen = false;
            showSystemUI();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (!isExpand){
            final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) findViewById(R.id.appbar).getLayoutParams();
            final AppBarLayout.Behavior behavior = new FlingBehavior();
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    params.setBehavior(behavior);
                }
            }, 500);
        }

        if (isForce && !isExpand){
            viewToResize.getLayoutParams().height = videoHeight;
            findViewById(R.id.back_btn_when_toolbar_hidden).setAlpha(1);
            findViewById(R.id.channel_logo).setAlpha(1);

            return;
        }
        final int initialHeight = videoHeight;
        final Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        final int windowHeight = size.y;
        final int delta = windowHeight - initialHeight;
        final float initTranslation = emVideoView.getTranslationY();

        Animation a = new Animation(){
            @SuppressWarnings("ResourceType")
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {

                float reflexedInterpolatedTime = isExpand ? (1 - interpolatedTime) : interpolatedTime;
                viewToResize.setBackgroundColor(Color.rgb((int) (76 * reflexedInterpolatedTime),
                        (int) (200 * reflexedInterpolatedTime),
                        (int) (226 * reflexedInterpolatedTime)));
                findViewById(R.id.back_btn_when_toolbar_hidden).setAlpha(reflexedInterpolatedTime);
                findViewById(R.id.channel_logo).setAlpha(reflexedInterpolatedTime);

                if (isExpand){
                    viewToResize.getLayoutParams().height = interpolatedTime == 1
                            ? windowHeight
                            : (int)(initialHeight + delta * interpolatedTime);
                } else {

                    viewToResize.getLayoutParams().height = interpolatedTime == 1
                            ? initialHeight
                            : (int)(windowHeight - delta * interpolatedTime);

                    emVideoView.setTranslationY(interpolatedTime == 1
                            ? 0
                            : (initTranslation * (1 - interpolatedTime)));
                }

                viewToResize.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return false;
            }
        };
        a.setDuration(500);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        viewToResize.startAnimation(a);
    }


    private void initPlayer(final boolean playWhenReady){
//        if (player == null){
//            new Retrofit.Builder()
//                    .baseUrl("http://www.youtube.com")
//                    .addConverterFactory(ScalarsConverterFactory.create())
//                    .build().create(MyApiEndpointInterface.class).getYoutubeManifest(thisVideoData.id).enqueue(this);
//        } else {
////            preparePlayer(playWhenReady);
//        }

        emVideoView = (EMVideoView) findViewById(R.id.video_view);
        emVideoView.setOnPreparedListener(this);
        emVideoView.setOnSeekCompletionListener(this);
        emVideoView.setScaleType(ScaleType.CENTER_CROP);

        controls.setMediaPlayer(emVideoView);
        controls.setPlayWhenReadyListener(this);
        Log.d("needed uri", emVideoView.getVideoUri() == null ? "null" : emVideoView.getVideoUri().toString());
        if (emVideoView.getVideoUri() != null && !isActivityLeft){
            showPreparedPlayer();
            return;
        }
        isActivityLeft = false;
        String youtubeLink = "http://youtube.com/watch?v=" +thisVideoData.id ;

        new YouTubeExtractor(this) {
            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null && ytFiles.get(22) != null) {
                    emVideoView.setVideoURI(Uri.parse(ytFiles.get(22).getUrl()));

                }
                else Log.d("***", "no url");
            }

        }.extract(youtubeLink, false, false);
    }

//    private void preparePlayer(final boolean playWhenReady){
//
//        Log.d("***", "player starts to prepare");
//        player.prepare();
//        Log.d("***", "player : player is reday");
////        player.setSurface(new Surface(surfaceView.getSurfaceTexture()));
//        Log.d("***", "player : surfaceView is reday");
//        player.setPlayWhenReady(playWhenReady);
//        player.setListener(this);
//        controls.hide();
//
//        controls.setEnabled(true);
//    }

    @Override
    protected void onStart() {
        super.onStart();

        initPlayer(true);
    }



    @SuppressLint("InlinedApi")
    private void hideSystemUI() {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View mDecorView = getWindow().getDecorView();
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI() {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View mDecorView = getWindow().getDecorView();
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        mDecorView.requestLayout();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VideoAdapter.REQUEST_VISITED_VIDEOS){
            if (resultCode == RESULT_OK){
                ArrayList<String> visitedVideos = data.getStringArrayListExtra(VideoAdapter.KEY_VISITED_NEWS);
                for (String newsId : visitedVideos){
                    for (VideoData videoData : otherFragment.dataset){
                        if (videoData.id.equals(newsId)){
                            videoData.views++;
                        }
                    }
                }
                visitedVideos.add(thisVideoData.id);
                otherFragment.visitedVideos = visitedVideos;
                ((VideoAdapter) otherFragment.recyclerView.getAdapter()).setDataset(otherFragment.dataset);
            }
        }
    }

    /* Need for drag video in portrait mode */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Only if video plays and orientational is portrait
        if (isFullscreen && lastOrientation != Configuration.ORIENTATION_LANDSCAPE){

            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    me_y = ev.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if( -me_y + ev.getY() > 50 || -me_y + ev.getY() < -50){
                        wasMovement = true;
//                        surfaceView.setTranslationY(-me_y + ev.getY());
                        emVideoView.setTranslationY(-me_y + ev.getY());
                        controls.hide();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (wasMovement){
                        stopPlayVideo();
                        wasMovement = false;
                    }
                    break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDrawerSliding(float slideOffset) {
        super.onDrawerSliding(slideOffset);
        controls.hide();
    }


    public void addComment(View view) {
        String sign = ((EditText) findViewById(R.id.addcomment_sign)).getText().toString();
        String message = ((EditText) findViewById(R.id.addcomment_message)).getText().toString();
        if (TextUtils.isEmpty(sign)){
            sign = "USER NAME";
        }
        if (TextUtils.isEmpty(message)){
            ((EditText) findViewById(R.id.addcomment_message)).setError(getString(R.string.need_to_fill_this_field));
            return;
        }

        Utils.saveUserName(this, sign);

        Callback<GeneralResponse> callback = new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.isSuccessful()){
                    //Cleans fields and hides keyboard

                    ((EditText) findViewById(R.id.addcomment_message)).setText("");

                    addCommentContainer.setVisibility(GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

                    View view = getCurrentFocus();
                    if (view == null) {
                        view = new View(PlayerActivity.this);
                    }
                    onCommentAddedListener.onCommentAdded();
                    view.clearFocus();
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {
                    Toast.makeText(PlayerActivity.this, "An error occured", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(PlayerActivity.this,
                        "Try again",
                        Toast.LENGTH_LONG).show();
            }

        };


        if (TextUtils.isEmpty(idToComment) || idToComment.equals(thisVideoData.id)){

            Log.d("***", "comment to video");
            apiService.addComment(thisVideoData.id,
                    new CommentRequest(
                            Utils.getDeviceId(PlayerActivity.this),
                            Utils.getEmail(PlayerActivity.this),
                            Utils.getToken(PlayerActivity.this),
                            message,
                            sign))
                    .enqueue(callback);
        } else {
            apiService.addCommentToComment(idToComment,
                    new CommentRequest(
                            Utils.getDeviceId(PlayerActivity.this),
                            Utils.getEmail(PlayerActivity.this),
                            Utils.getToken(PlayerActivity.this),
                            message,
                            sign))
                    .enqueue(callback);
        }

    }

    public void showAddCommentView(String commentId){

        idToComment = commentId;

        ((EditText) findViewById(R.id.addcomment_sign)).setText(Utils.getUserName(this));
        addCommentContainer.setVisibility(View.VISIBLE);
        findViewById(R.id.addcomment_message).requestFocus();

        InputMethodManager mgr = (InputMethodManager) PlayerActivity.this.getSystemService(INPUT_METHOD_SERVICE);
        mgr.showSoftInput(findViewById(R.id.addcomment_message), InputMethodManager.SHOW_IMPLICIT);
    }

    public void showAddCommentView(){

        showAddCommentView(thisVideoData.id);
    }

    public void scrollViewPager(View view) {
        int id = view.getId();
        int pageToScroll = 0;
//        PorterDuffColorFilter white = new PorterDuffColorFilter(Color.WHITE,
//                PorterDuff.Mode.SRC_ATOP);
//        PorterDuffColorFilter blueGray = new PorterDuffColorFilter(Color.parseColor("#3B98AC"),
//                PorterDuff.Mode.SRC_ATOP);
        if (id == R.id.navig_details){

//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_other_videos)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_comments)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) view).getChildAt(0)).setColorFilter(white);
            pageToScroll = 0;
        } else if (id == R.id.navig_other_videos){

//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_details)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_comments)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) view).getChildAt(0)).setColorFilter(white);
            pageToScroll = 1;
        } else if (id == R.id.navig_comments){


//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_other_videos)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) findViewById(R.id.navig_details)).getChildAt(0)).setColorFilter(blueGray);
//            ((ImageView) ((ViewGroup) view).getChildAt(0)).setColorFilter(white);
            pageToScroll = 2;
        }
        viewPager.setCurrentItem(pageToScroll, true);

    }

    private int getLikeCount(){
        return thisVideoData.voted.equals(DetailsFragment.LIKED) ? thisVideoData.likes + 1 : thisVideoData.likes;
    }
    private int getDislikeCount(){
        return thisVideoData.voted.equals(DetailsFragment.DISLIKED) ? thisVideoData.dislikes + 1 : thisVideoData.dislikes;
    }

    @Override
    public void onVideoDataLoaded(VideoData videoData) {
        thisVideoData = videoData;
        panel.setVisibility(View.VISIBLE);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        try {
            Date date = sdf.parse(videoData.publishedAt);
            sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            ((TextView) findViewById(R.id.panel_date)).setText(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ((TextView) findViewById(R.id.panel_views)).setText(String.format(Locale.getDefault(), "%d", videoData.views));
        ((TextView) findViewById(R.id.panel_title)).setText(videoData.title);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (emVideoView.isPlaying()){
                    stopPlayVideo();
                } else {
                    startPlayVideo();
                }
            }
        });
    }

    public void setOnVideoPlayedListener(OnVideoPlayedListener onVideoPlayedListener) {
//        this.onVideoPlayedListener = onVideoPlayedListener;
    }

    public void editUserName(View view) {
        Log.d("***", "editUserName");

        InputMethodManager mgr = (InputMethodManager) PlayerActivity.this.getSystemService(INPUT_METHOD_SERVICE);
        mgr.showSoftInput(findViewById(R.id.addcomment_sign), InputMethodManager.SHOW_IMPLICIT);
    }
//
//    @Override
//    public void onResponse(Call<String> call, Response<String> response) {
//        if (response.isSuccessful()){
////            player = new YoutubeExoPlayer(new DashRendererBuilder(this, Utils.getDashFromManifest(response.body())));
//////            preparePlayer(false);
//            emVideoView.setVideoURI(Uri.parse(Utils.getDashFromManifest(response.body())));
//
//        }
//    }
//
//    @Override
//    public void onFailure(Call<String> call, Throwable t) {
//        t.printStackTrace();
//    }

    @Override
    public void onPrepared() {
        Log.d("***", "onPrepared");

        if (lastPosition != 0){
            emVideoView.seekTo(lastPosition);
            return;
        }

        showPreparedPlayer();
    }

    private void showPreparedPlayer(){

        playButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(GONE);
        startResizeAnimation(videoContainer, false, true);
        videoContainer.invalidate();
        videoContainer.requestLayout();
        panel.requestLayout();
        panel.invalidate();
        panel.setVisibility(View.GONE);
        panel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSeekComplete() {

        Log.d("***", "onPreparedonSeekComplete");
        if (!lastPlayerState)
            showPreparedPlayer();
    }
//

    public class UnderVideoPagerAdapter extends FragmentStatePagerAdapter {
        public UnderVideoPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i == 0) {
                DetailsFragment detailsFragment = DetailsFragment.newInstance(thisVideoData.id, thisVideoData.channel.id, thisVideoData.title, thisVideoData.channel.humbnails._60x60);
                detailsFragment.setOnVideoDataLoadedListener(PlayerActivity.this);
                return detailsFragment;
            } else if (i == 1) {
                OtherVideosFragment otherVideosFragment = OtherVideosFragment.newInstance(thisVideoData.id, thisVideoData.channel.id, thisVideoData.title);
                setOnVideoPlayedListener(otherVideosFragment);
                otherFragment = otherVideosFragment;
                return otherVideosFragment;
            }
            CommentsFragment commentsFragments = CommentsFragment.newInstance(thisVideoData.id);
            onCommentAddedListener = commentsFragments;
            return commentsFragments;
        }



        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            SpannableStringBuilder sb = new SpannableStringBuilder(" "); // space added before text for convenience
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            Drawable myDrawable = null;


            switch (position){
                case 0:

                    myDrawable = ContextCompat.getDrawable(PlayerActivity.this, R.drawable.info);
                    break;
                case 1:

                    myDrawable = ContextCompat.getDrawable(PlayerActivity.this, R.drawable.videos);
                    break;
                case 2:

                    myDrawable = ContextCompat.getDrawable(PlayerActivity.this, R.drawable.comments);

                    break;
            }
            myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(), myDrawable.getIntrinsicWidth());

            ImageSpan span = new ImageSpan(myDrawable, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(span, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            return sb;
        }

    }

    public interface OnVideoPlayedListener {
        void onPlayed(boolean isPlayed);
    }


    public interface OnCommentAddedListener{
        void onCommentAdded();
    }

    private void startPlayVideo(){
        if (lastOrientation != Configuration.ORIENTATION_LANDSCAPE)
            startResizeAnimation(videoContainer, true, false);
        panel.setVisibility(GONE);
        playButton.setVisibility(GONE);
        controls.play();

        if (!wasMarkAsViewed){
            markAsViewed();
        }
    }
    private void stopPlayVideo(){
        lastPosition = emVideoView.getCurrentPosition();
        startResizeAnimation(videoContainer, false, false);
        playButton.setVisibility(View.VISIBLE);
        controls.pause();
    }

    public void onPreActivityLeft(){
        emVideoView.stopPlayback();
        isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        emVideoView.release();
        super.onDestroy();
    }
}
