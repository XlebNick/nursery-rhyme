package net.magtoapp.a.activity;

import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.server.MyApiEndpointInterface;

public class VideoFeedActivity extends CategoryActivity {

    private boolean isFirstTime = true;
    @Override
    public boolean loadData() {

        type = ActivityType.VideoFeedActivity;
        setExtraBackButtonVisible(false);
        if (isFirstTime)
            showSplashScreenWindow(true);
        isFirstTime = false;
        retrofit.create(MyApiEndpointInterface.class).getAllVideos(Utils.getAppId(this)).enqueue(this);
        return true;
    }
}
