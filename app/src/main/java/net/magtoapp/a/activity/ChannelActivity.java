package net.magtoapp.a.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.Channels;
import net.magtoapp.a.recyclervieweffects.LargeTopItemOnScrollListener;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.view.ScrollViewWithMaxHeight;


public class ChannelActivity extends TemplateActivity {

//    private VerticalViewPager viewPager;
    private RecyclerView recyclerView;
    private String channelId;
    private String channelTitle;
    private String channelDescription;
    private ScrollViewWithMaxHeight scrollView;
    private ArrayList<VideoData> dataset;
    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_channel);
        setExtraBackButtonVisible(true);
        setPaddingContent(0, 0, 0, 0);
        searchEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.videos_listview);
//        LargeTopItemOnScrollListener listener = new LargeTopItemOnScrollListener();
//        listener.setOffset(1, 0);
//        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
//        SnapHelper snapHelperTop = new LinearSnapHelper();
//        snapHelperTop.attachToRecyclerView(recyclerView);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeLayout.setColorSchemeColors(Color.rgb(76, 200, 226));
        swipeLayout.setOnRefreshListener(this);
        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());


        recyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {
            boolean mFirstOnTouchEvent;
            // stores ScrollView coordinates relative to RecyclerView
            private final Rect bounds = new Rect();

            @Override public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                final int x = (int) e.getRawX();
                final int y = (int) e.getRawY();


                if (e.getAction() == MotionEvent.ACTION_DOWN){
                    mFirstOnTouchEvent = true;
                }

                // Getting it's coordinates relative to root (or RecyclerView in my case)
                if (scrollView != null){
                    scrollView.getGlobalVisibleRect(bounds);

                    bounds.top = 0;
                    // If touch location is inside ScrollView bounds we consume it
                    if (x > bounds.left && x < bounds.right && y >bounds.top && y < bounds.bottom) {
                        // Android offsets touch locations, so and we do

                        e.offsetLocation(-bounds.left, -bounds.top);
                        return scrollView.onInterceptTouchEvent(e);
                    } else {
                        return super.onInterceptTouchEvent(rv, e);
                    }
                }

                return super.onInterceptTouchEvent(rv, e);

            }

            @Override public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                if (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP) {
                    mFirstOnTouchEvent = false;
                }

                if (mFirstOnTouchEvent) {
                    e.setAction(MotionEvent.ACTION_DOWN);
                    e.setLocation(e.getX(), e.getY());
                    mFirstOnTouchEvent = false;
                }
                // Again, Android offsets touch locations, so and we do
                e.offsetLocation(-bounds.left, -bounds.top);
                scrollView.dispatchTouchEvent(e);
            }
        });


        channelId = getIntent().getExtras().getString("id");
        channelTitle = getIntent().getExtras().getString("title");
        channelDescription = getIntent().getExtras().getString("description");
        setTitle(channelTitle);
        setIsLocalSearch(true);

    }


    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;


    @Override
    public boolean loadData() {
        MyApiEndpointInterface apiService = retrofit.create(MyApiEndpointInterface.class);

        Call<Channels.Channel> getChannelInfoCall = apiService.getChannelInfo(channelId,
                Utils.getAppId(this));
        getChannelInfoCall.enqueue(new Callback<Channels.Channel>() {
            @Override
            public void onResponse(Call<Channels.Channel> call, Response<Channels.Channel> response) {
                if(response.isSuccessful()){
                    if (response.body() != null) {
                        channelTitle = response.body().title;
                        channelDescription = response.body().description;
                    }
                }
            }

            @Override
            public void onFailure(Call<Channels.Channel> call, Throwable t) {

            }
        });

        Call<ArrayList<VideoData>> call = apiService.getAllVideosFromChannel(channelId,
                Utils.getAppId(this));
        call.enqueue(new Callback<ArrayList<VideoData>>() {

            @Override
            public void onResponse(Call<ArrayList<VideoData>> call, Response<ArrayList<VideoData>> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().size() > 0){
                        showContent();
                        ArrayList<VideoData> receivedItems = response.body();
                        for (VideoData videoData : receivedItems){
                            videoData.channel.id = channelId;
                        }
                        dataset = receivedItems;
                        ChannelVideoAdapter adapter = new ChannelVideoAdapter(ChannelActivity.this, dataset);
                        adapter.setDataSetOffSet(1, 0);
                        recyclerView.setAdapter(adapter);

                    } else {
                        showNothingFoundWindow();
                    }
                } else {
                    showErrorOccuredWindow();
                }

                swipeLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<VideoData>> call, Throwable t) {
                t.printStackTrace();
                showErrorOccuredWindow();

                swipeLayout.setRefreshing(false);
            }

        });
        return true;
    }


    @Override
    public void performSearch() {
        if (dataset == null || dataset.size() == 0){
            showNothingFoundWindow();
            return;
        }
        List<VideoData> searchResults = new ArrayList<>();
        String searchPhrase = searchEditText.getText().toString();
        for (VideoData videoData : dataset){
            if (videoData.description.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.title.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.channel.title.toLowerCase().contains(searchPhrase.toLowerCase()))
                searchResults.add(videoData);
        }

        if (searchResults.size() != 0 ){

            ((RecyclerView) findViewById(R.id.search_result_videos_list)).setAdapter(new VideoAdapter(ChannelActivity.this, searchResults));
            showSearchResult();
        } else {
            showNothingFoundWindow();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        refresh(getCurrentFocus());


        swipeLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (recyclerView.getScrollY() == 0)
                            swipeLayout.setEnabled(true);
                        else
                            swipeLayout.setEnabled(false);

                    }
                });
        recyclerView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = recyclerView.getScrollY();
                if(scrollY == 0) swipeLayout.setEnabled(true);
                else swipeLayout.setEnabled(false);

            }
        });

    }

    private class ChannelVideoAdapter extends VideoAdapter{

        static final int TYPE_DESCRIPTION = 2;

        ChannelVideoAdapter(Context context, List<VideoData> objects) {
            super(context, objects);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_DESCRIPTION){
                View view = View.inflate(getBaseContext(), R.layout.channel_description, null);
                return new RecyclerView.ViewHolder(view) {};
            }
            return super.onCreateViewHolder(parent, viewType);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder.itemView.findViewById(R.id.description) != null){
                ((TextView) viewHolder.itemView.findViewById(R.id.description)).setText(channelDescription);
                scrollView = (ScrollViewWithMaxHeight) viewHolder.itemView.findViewById(R.id.scroll_view);

                scrollView.setMaxHeight(200);
                if (TextUtils.isEmpty(channelDescription)){
                    scrollView.setVisibility(View.GONE);
                } else {
                    scrollView.setVisibility(View.VISIBLE);
                }
                viewHolder.setIsRecyclable(false);
//                ViewTreeObserver vto = scrollView.getViewTreeObserver();
//                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//
//                    @Override
//                    public void onGlobalLayout() {
//
//                        scrollView.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                        if (scrollView.getMeasuredHeight() >  200){
//                            scrollView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 200));
//                        } else {
//
//                            scrollView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//                        }
//
//                        ViewTreeObserver obs = scrollView.getViewTreeObserver();
//
////                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
////                            obs.removeOnGlobalLayoutListener(this);
////                        } else {
////                            obs.removeGlobalOnLayoutListener(this);
////                        }
//                    }
//
//                });

            }
            super.onBindViewHolder(viewHolder, position);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return TYPE_DESCRIPTION;
            return super.getItemViewType(position);
        }

    }




}
