package net.magtoapp.a.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.recyclervieweffects.LargeTopItemOnScrollListener;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.controller.Utils;
import trikita.log.Log;

public class StarredActivity extends TemplateActivity {

    private RecyclerView recyclerView;
    private ArrayList<VideoData> dataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_history);
        setPaddingContent(0, 0, 0, 0);
        setExtraBackButtonVisible(true);
        type = ActivityType.StarredActivity;

        setTitle("Bookmarks");
        setIsLocalSearch(true);
        searchEnabled(true);

        recyclerView = ((RecyclerView)findViewById(R.id.history_list));
        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
//        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
//        SnapHelper snapHelperTop = new LinearSnapHelper();
//        snapHelperTop.attachToRecyclerView(recyclerView);
//
//        ((TextView) findViewById(R.id.label)).setText("Bookmarks");
    }



    @Override
    public boolean loadData() {
        dataset = Utils.getStarredItems(this);
        Collections.sort(dataset, new Comparator<VideoData>() {
            @Override
            public int compare(VideoData historyItem, VideoData otherHistoryItem) {
                return (int) (otherHistoryItem.timeOfWatching - historyItem.timeOfWatching);
            }
        });

        Log.d(dataset.size());
        VideoAdapter adapter = new VideoAdapter(this, dataset);
        recyclerView.setAdapter(adapter);
        showContent();
        return true;
    }

    @Override
    public void performSearch() {
        if (dataset == null || dataset.size() == 0){
            showNothingFoundWindow();
            return;
        }
        List<VideoData> searchResults = new ArrayList<>();
        String searchPhrase = searchEditText.getText().toString();
        for (VideoData videoData : dataset){
            if (videoData.description.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.title.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.channel.title.toLowerCase().contains(searchPhrase.toLowerCase()))
                searchResults.add(videoData);
        }

        if (searchResults.size() != 0 ){

            ((RecyclerView) findViewById(R.id.search_result_videos_list)).setAdapter(new VideoAdapter(StarredActivity.this, searchResults));
            if (shouldCloseAfterSearch)
                showSearchResult();
        } else {
            showNothingFoundWindow();
        }


    }


}
