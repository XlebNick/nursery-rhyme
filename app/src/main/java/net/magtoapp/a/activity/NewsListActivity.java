package net.magtoapp.a.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import net.magtoapp.a.adapters.NewsAdapter;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.News;

public class NewsListActivity extends TemplateActivity implements Callback<ArrayList<News>> {

    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_news_list);

        setWhileLineVisible(false);
        setExtraBackButtonVisible(true);
        setTitle(R.string.news_item_title);
        setPaddingContent(0, 0, 0, 0);

        type = ActivityType.NewsListActivity;

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new NewsAdapter(new ArrayList<News>(), this));


        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeLayout.setColorSchemeColors(Color.rgb(76, 200, 226));
        swipeLayout.setOnRefreshListener(this);

    }


    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    @Override
    public void onStart() {
        super.onStart();

        swipeLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (recyclerView.getScrollY() == 0)
                            swipeLayout.setEnabled(true);
                        else
                            swipeLayout.setEnabled(false);

                    }
                });
        recyclerView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = recyclerView.getScrollY();
                if(scrollY == 0) swipeLayout.setEnabled(true);
                else swipeLayout.setEnabled(false);

            }
        });
    }


    @Override
    public boolean loadData() {

        retrofitXml.create(MyApiEndpointInterface.class).getNews(1, 100,
                Utils.getAppId(this)).enqueue(this);

        return true;
    }

    @Override
    public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {

        if (response.isSuccessful()){
            if (response.body() != null && response.body().size() > 0){
                showContent();

                Set<String> visitedNewsIds = Utils.getVisitedNews(NewsListActivity.this);
                ArrayList<News> dataSet = response.body();
                for (News  news : dataSet){
                    if (visitedNewsIds.contains(news.link)){
                        news.isVisited = true;
                    }
                }
                ((NewsAdapter) recyclerView.getAdapter()).setDataSet(dataSet);
            } else {
                showNothingFoundWindow();
            }
        } else {
            showErrorOccuredWindow();
        }

        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<ArrayList<News>> call, Throwable t) {
        showErrorOccuredWindow();
        t.printStackTrace();

        swipeLayout.setRefreshing(false);
    }
}
