package net.magtoapp.a.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.model.VideoCollection;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.server.StupidXmlConverterFactory;
import net.magtoapp.a.controller.Utils;
import trikita.log.Log;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.View.GONE;
import static java.util.Calendar.HOUR_OF_DAY;

/**
 * Created by XlebNick for carousel-mainpage-test.
 */
public abstract class TemplateActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    boolean isOnline;

    public static final String BASE_URL = "https://media.magtoapp.net/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("dd-MM-yyyy hh:mm:ss ZZZ").create()))
            .build();
    Retrofit retrofitXml = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(new StupidXmlConverterFactory())
            .build();

    protected View innerToolbar;
//    private View appBarDivider;
    private ImageView backButton;
    private View toolbar;
    private ImageView menuButton;
    private ImageView homeButton;
     ListView navDrawerItems;
    private DrawerLayout navigationDrawer;
    private CardView drawerContainer;
    private LinearLayout container;

    private View plane;

    /**VIewGroup that contains EditText to enter search phrase, clean/close/search buttons and
     * shadow for content*/
    protected View searchBar;
    /**EditText to enter search phrase*/
    protected EditText searchEditText;
    /**Contains title with search phrase and back button*/
    private View searchResultBar;
    private RecyclerView searchResultList;

    boolean isActivityInFront = false;

    protected boolean isSplashScreenVisible;
    protected boolean shouldCloseAfterSearch = false;
    private boolean isFirstTime = true;
    private boolean shouldShowProgressBar = true;
    private boolean shouldPlaneFly = true;
    private Animation planeAnimation;

    protected ArrayList<String> menuItems;

    protected enum ActivityType {
        MainActivity, VideoFeedActivity, HistoryActivity, StarredActivity, NewsListActivity, ChannelsListActivity
    }
    
    protected ActivityType type;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);
        navDrawerItems = (ListView) findViewById(R.id.nav_drawer_listview);
        navigationDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        container = (LinearLayout) findViewById(R.id.main_content);
        drawerContainer = (CardView) findViewById(R.id.drawer_container);
        toolbar = findViewById(R.id.toolbar);
        innerToolbar = findViewById(R.id.inner_toolbar);
        backButton = (ImageView) findViewById(R.id.back_btn);
        menuButton = (ImageView) findViewById(R.id.menu_btn);
        plane = findViewById(R.id.plane);

        homeButton = (ImageView) findViewById(R.id.home_button);

        if (type == ActivityType.MainActivity )
            homeButton.setVisibility(View.GONE);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), CategoryListActivity.class));
            }
        });

        searchBar = findViewById(R.id.search_bar);
        searchEditText = (EditText) findViewById(R.id.search_view);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    prepareAndPerformSearch(getCurrentFocus());
                    return true;
                }
                return false;
            }
        });


        
        searchResultBar = findViewById(R.id.searchResult);
        searchResultList = (RecyclerView) findViewById(R.id.search_result_videos_list);

        Glide.with(this).load(R.drawable.main_background)
                .centerCrop()
                .into((ImageView) findViewById(R.id.template_background));
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(System.currentTimeMillis());

        int backgroundRes = 0;
        if (date.get(HOUR_OF_DAY) < 6 || date.get(HOUR_OF_DAY) >= 20)
            backgroundRes = R.drawable.night_background;
        else if (date.get(HOUR_OF_DAY) >= 6 && date.get(HOUR_OF_DAY) < 11)
            backgroundRes = R.drawable.morning_background;
        else if (date.get(HOUR_OF_DAY) >= 11 && date.get(HOUR_OF_DAY) < 20)
            backgroundRes = R.drawable.day_background;

        Glide.with(this).load(backgroundRes)
                .centerCrop()
                .into((ImageView) findViewById(R.id.template_back_background));
        Glide.with(this).load(backgroundRes)
                .centerCrop()
                .into((ImageView) findViewById(R.id.template_background));


        drawerContainer.setMaxCardElevation(0);
        View drawerSlider = findViewById(R.id.drawer_slider);
        drawerSlider.getLayoutParams().width = (int) (getResources().getDisplayMetrics().widthPixels * 0.6f);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, navigationDrawer, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

//                drawerContainer.setScaleX(1 - (slideOffset / 4));
//                drawerContainer.setScaleY(1 - (slideOffset / 4));;
                drawerContainer.setTranslationX(slideOffset * drawerView.getWidth() * 1.5f);
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                View view = getCurrentFocus();
                if (view == null) {
                    view = new View(TemplateActivity.this);
                }
                view.clearFocus();
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                drawerContainer.setMaxCardElevation(Math.round(slideOffset * 6));
//                drawerContainer.setCardElevation(Math.round(slideOffset * 6));
                toolbar.setTranslationY((-1 + slideOffset) * toolbar.getHeight());
                ((View) navDrawerItems.getParent()).setTranslationY(toolbar.getHeight() * 1.5f);

                if (slideOffset == 1){
                    startPlaneAnimation();
                } else {

                    stopPlaneAnimation();
                }


                onDrawerSliding(slideOffset);
            }
        };
        menuItems = new ArrayList<>();
        final String[] initialItems =   getResources().getStringArray(R.array.drawer_items);
        Collections.addAll(menuItems, initialItems);
        if (getResources().getBoolean(R.bool.has_news) && getResources().getBoolean(R.bool.show_news_in_menu)){
            menuItems.add(1, getString(R.string.news_item_title));
        }
        if (getResources().getBoolean(R.bool.multi_authors)){
            menuItems.add(1, getString(R.string.authors_item_title));
        }
        navDrawerItems.setDividerHeight(40);
        navigationDrawer.setDrawerListener(drawerToggle);
        navigationDrawer.setScrimColor(Color.TRANSPARENT);
        navDrawerItems.setTranslationY(findViewById(R.id.appbarlayout).getHeight());
        navDrawerItems.setAdapter(new ArrayAdapter<>(
                this,
                R.layout.nav_drawer_listview_item,
                menuItems));
        navDrawerItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (menuItems.get(position).equals(initialItems[0])){
                    if (type != ActivityType.VideoFeedActivity) {
                        Intent intent = new Intent(getApplicationContext(), VideoFeedActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                } else if (menuItems.get(position).equals(initialItems[1])){
                    if (type != ActivityType.HistoryActivity) {

                        Intent intent = new Intent(getApplicationContext(), CategoryListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                } else if (menuItems.get(position).equals(initialItems[2])){
                    if (type != ActivityType.StarredActivity) {

                        Intent intent = new Intent(getApplicationContext(), StarredActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                }else if (menuItems.get(position).equals(initialItems[3])){
                    if (type != ActivityType.HistoryActivity) {

                        Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                }else if (menuItems.get(position).equals(initialItems[4])){

                    navigationDrawer.closeDrawer(GravityCompat.START);
                    showSearchBar(getCurrentFocus());
                } else if (menuItems.get(position).equals(getString(R.string.news_item_title))){
                    if (type != ActivityType.NewsListActivity) {

                        Intent intent = new Intent(getApplicationContext(), NewsListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                } else if (menuItems.get(position).equals(getString(R.string.authors_item_title))){
                    if (type != ActivityType.ChannelsListActivity) {

                        Intent intent = new Intent(getApplicationContext(), ChannelsListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        navigationDrawer.closeDrawer(GravityCompat.START);
                    }
                }
//                switch (position) {
//                    case 0:
//
//                        break;
//                    case 1:
//                        if (type != ActivityType.ChannelActivity) {
//
//                            Intent intent = new Intent(getApplicationContext(), ChannelsListActivity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
//                        } else {
//                            navigationDrawer.closeDrawer(GravityCompat.START);
//                        }
//                        break;
//                    case 2:
//                        if (type != ActivityType.HistoryActivity) {
//
//                            Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
//                        } else {
//                            navigationDrawer.closeDrawer(GravityCompat.START);
//                        }
//                        break;
//                    case 3:
//                        if (type != ActivityType.StarredActivity) {
//
//                            Intent intent = new Intent(getApplicationContext(), StarredActivity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
//                        } else {
//                            navigationDrawer.closeDrawer(GravityCompat.START);
//                        }
//                        break;
//                    case 4:
//                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
//                        break;
//
//                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    protected void hideHomeButton() {
        homeButton.setVisibility(GONE);
    }

    public void onMenuButtonPressed(View v){

        if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
            navigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            navigationDrawer.openDrawer(GravityCompat.START);
        }
    }
    private void startPlaneAnimation(){

        shouldPlaneFly = true;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                float initTranslation = plane.getTranslationY();
                float newTranslation = new Random().nextFloat() * 20 - 10;
                planeAnimation = new TranslateAnimation(0, 0, initTranslation, newTranslation);
                planeAnimation.setDuration(100);
                plane.startAnimation(planeAnimation);

                if (shouldPlaneFly)
                handler.postDelayed(this, 100);
            }
        }, 100);




    }

    private void stopPlaneAnimation(){

        shouldPlaneFly = false;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void setIsLocalSearch(boolean isLocalSearch){

        if (isLocalSearch){

            searchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    performSearch();
                    findViewById(R.id.clear_btn).setVisibility(s.length() == 0 ? GONE : View.VISIBLE);

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    @Override
    public void onRefresh() {
        shouldShowProgressBar = false;
        refresh(getCurrentFocus());
    }

    public void searchEnabled(boolean isEnabled){

        findViewById(R.id.white_search_btn).setVisibility(isEnabled ? View.VISIBLE : View.GONE);
        if (isEnabled){
//            searchResultList.addOnScrollListener(new LargeTopItemOnScrollListener());
            searchResultList.setAdapter(new VideoAdapter(this, new ArrayList<VideoData>()));

//            SnapHelper snapHelperTop = new LinearSnapHelper();
//            snapHelperTop.attachToRecyclerView(searchResultList);
        }
    }

    public void showContent(){
        findViewById(R.id.error_occured_view).setVisibility(View.GONE);
        findViewById(R.id.splash_view).setVisibility(View.GONE);
        findViewById(R.id.nothing_found_view).setVisibility(View.GONE);
        searchResultList.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
    }

    public void showErrorOccuredWindow(){
        findViewById(R.id.error_occured_view).setVisibility(View.VISIBLE);
        findViewById(R.id.splash_view).setVisibility(View.GONE);
        findViewById(R.id.nothing_found_view).setVisibility(View.GONE);
        searchResultList.setVisibility(View.GONE);
        container.setVisibility(View.GONE);
    }

    public void showNothingFoundWindow(){
        findViewById(R.id.error_occured_view).setVisibility(View.GONE);
        findViewById(R.id.splash_view).setVisibility(View.GONE);
        findViewById(R.id.nothing_found_view).setVisibility(View.VISIBLE);
        searchResultList.setVisibility(View.GONE);
        container.setVisibility(View.GONE);
    }

    public void showSearchResult(){

        findViewById(R.id.error_occured_view).setVisibility(View.GONE);
        findViewById(R.id.splash_view).setVisibility(View.GONE);
        findViewById(R.id.nothing_found_view).setVisibility(View.GONE);
        container.setVisibility(View.GONE);
        searchResultList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTitle(CharSequence title) {
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/BUBBLEGUMS.TTF");
        ((TextView) findViewById(R.id.appbar_title)).setTypeface(type);

        ((TextView) findViewById(R.id.appbar_title)).setText(((String) title).toUpperCase());
    }

    public void showSplashScreenWindow(){
        showSplashScreenWindow(false);
    }

    public void showSplashScreenWindow(boolean isBackgroundImageRequired){
        Log.d("***", isBackgroundImageRequired + "");
        if (shouldShowProgressBar){

            this.isSplashScreenVisible = true;
            findViewById(R.id.error_occured_view).setVisibility(View.GONE);
            findViewById(R.id.nothing_found_view).setVisibility(View.GONE);
            findViewById(R.id.splash_view).setVisibility(View.VISIBLE);

            if (isBackgroundImageRequired){
                findViewById(R.id.splash_image).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.splash_image).setVisibility(View.GONE);
            }

        }
        shouldShowProgressBar = true;

    }

    public void showSearchBar(View view) {
        searchBar.setAlpha(0);
        searchBar.setVisibility(View.VISIBLE);
        searchBar.bringToFront();
        searchEditText.requestFocus();
        if (isOnline)
            ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                .showSoftInput(searchEditText, InputMethodManager.SHOW_FORCED);
        searchBar.animate()
                .alpha(1)
                .setDuration(200)
                .setListener(null);

    }

    @Override
    public void onBackPressed() {
        if (searchBar.getVisibility() == View.VISIBLE) {
            hideSearchBar(searchBar);
            hideSearchResult(searchBar);
        } else {
            super.onBackPressed();
        }
    }

    public void hideSearchBar(final View view) {
        ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(view.getWindowToken(), 0);
        searchBar.animate()
                .alpha(0)
                .setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        searchBar.setVisibility(View.GONE);
                    }
                });


    }

    public void clearSearchView(View view) {
        searchEditText.setText("");
        hideSearchResult(view);
        hideSearchBar(view);
    }

    public void hideSearchResult(View view){

        if (searchResultBar.getVisibility() == View.VISIBLE){
            searchResultBar.setVisibility(View.GONE);
        }

        
        showContent();
    }

    public void performSearch(){
        final Call<VideoCollection> searchVideos = retrofit
                .create(MyApiEndpointInterface.class)
                .searchVideos(searchEditText.getText().toString(), Utils.getAppId(this));

        searchVideos.enqueue(new Callback<VideoCollection>() {
            @Override
            public void onResponse(Call<VideoCollection> call, Response<VideoCollection> response) {
                if (response.body() != null && response.body().videos != null && response.body().videos.collection != null &&response.body().videos.collection.size() > 0 ){

                    ((VideoAdapter) searchResultList
                            .getAdapter()).setDataset(response.body().videos.collection);
                    showSearchResult();
                } else {

                    Log.d("perform search fail");

                    showNothingFoundWindow();
                }
            }

            @Override
            public void onFailure(Call<VideoCollection> call, Throwable t) {

                t.printStackTrace();
                showNothingFoundWindow();
            }

        });
    }

    public final void prepareAndPerformSearch(final View view) {
        if (TextUtils.isEmpty(searchEditText.getText().toString())){
            if (view != null && view.getId() == R.id.blue_search_btn){
                hideSearchBar(view);
                hideSearchResult(view);
            }
            return;
        }

        shouldCloseAfterSearch = true;

        hideSearchBar(getCurrentFocus());
        searchResultBar.setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.searchQuery)).setText(searchEditText.getText().toString());
        showSplashScreenWindow();
        performSearch();


//        final Call<ArrayList<News>> searchNews = retrofitXml
//                .create(MyApiEndpointInterface.class)
//                .searchNews(searchEditText.getText().toString());
//
//        searchNews.enqueue(new Callback<ArrayList<News>>() {
//            @Override
//            public void onResponse(Call<ArrayList<News>> call, Response<ArrayList<News>> response) {
//                if (response.isSuccessful()){
//                    findViewById(R.id.search_result).setVisibility(View.VISIBLE);
//                    findViewById(R.id.search_result).setAlpha(1);
//                    hideSearchBar(getCurrentFocus());
//                    searchResultBar.setVisibility(View.VISIBLE);
//                    ((TextView) findViewById(R.id.searchQuery)).setText(searchEditText.getText().toString());
//
//                    if (response.body() != null && response.body().size() > 0){
//                        findViewById(R.id.search_result_news_list).setVisibility(View.VISIBLE);
//                        findViewById(R.id.search_result_news_empty).setVisibility(View.GONE);
//                        Set<String> visitedNewsIds = Utils.getVisitedNews(TemplateActivity.this);
//                        ArrayList<News> dataSet = response.body();
//                        for (News  news : dataSet){
//                            if (visitedNewsIds.contains(news.link)){
//                                news.isVisited = true;
//                            }
//                        }
//                        ((RecyclerView) findViewById(R.id.search_result_news_list)).setAdapter(new NewsAdapter(response.body(), TemplateActivity.this ));
//                    } else {
//
//                        findViewById(R.id.search_result_news_list).setVisibility(View.GONE);
//                        findViewById(R.id.search_result_news_empty).setVisibility(View.VISIBLE);
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ArrayList<News>> call, Throwable t) {
//
//            }
//
//        });
    }

    protected void onDrawerSliding(float slideOffset){}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            navigationDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            navigationDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    public void setMainContentView(@LayoutRes int resId){

        View view = View.inflate(this, resId, null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        container.addView(view);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public boolean loadData(){
        showContent();
        return true;
    }

    public void refresh(View v){
        isOnline = isOnline();
        if (!isOnline()){
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG).show();
            onConnectionFailed();
            showErrorOccuredWindow();
        } else {
            showSplashScreenWindow();
            loadData();
        }

    }

    private void onConnectionFailed(){}

    public void setWhileLineVisible(boolean isVisible){
//        appBarDivider.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setExtraBackButtonVisible(boolean isVisible) {
        backButton.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        menuButton.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    public void setPaddingContent(int left, int top, int right, int bottom) {
        drawerContainer.getChildAt(0).setPadding(left, top, right, bottom);
    }


    @Override
    protected void onStop() {
        navigationDrawer.closeDrawer(GravityCompat.START);

        super.onStop();
    }

    public void showToolbar(boolean show){
        if (show){
            findViewById(R.id.inner_toolbar).setVisibility(View.VISIBLE);
//            findViewById(R.id.back_btn_when_toolbar_hidden).setVisibility(View.GONE);
        } else {
            findViewById(R.id.inner_toolbar).setVisibility(View.GONE);
//            findViewById(R.id.back_btn_when_toolbar_hidden).setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if(isFirstTime){
            refresh(getCurrentFocus());
        }
        isFirstTime = false;
        isActivityInFront = true;
    }
    @Override
    protected void onPause() {
        super.onPause();
        isActivityInFront = false;
    }



}
