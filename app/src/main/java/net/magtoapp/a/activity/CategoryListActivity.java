package net.magtoapp.a.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.Categories;
import trikita.log.Log;

public class CategoryListActivity extends TemplateActivity implements Callback<Categories>, SwipeRefreshLayout.OnRefreshListener, View.OnTouchListener {

//    private RecyclerView categoryList;
    private int changedPosition;

    private boolean multiAuthors;
    private boolean hasNews;


    public ArrayList<Categories.Category> categories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_category_list);
        hideHomeButton();
        setExtraBackButtonVisible(true);

        multiAuthors = getResources().getBoolean(R.bool.multi_authors);
        hasNews = getResources().getBoolean(R.bool.has_news);

        type = ActivityType.MainActivity;
        setWhileLineVisible(true);
        setPaddingContent(0, 0, 0, 0);
        searchEnabled(true);


        findViewById(R.id.clouds).setOnTouchListener(this);
        ((ImageView) findViewById(R.id.clouds)).setImageBitmap(drawTextToBitmap());
        findViewById(R.id.white_search_btn).setVisibility(View.VISIBLE);
        searchBar = findViewById(R.id.search_bar);

        findViewById(R.id.clouds).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

            }
        });

        ((AppBarLayout.LayoutParams) ((ViewGroup) findViewById(R.id.appbarlayout)).getChildAt(0).getLayoutParams())
                .setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);

    }

    @Override
    public boolean loadData() {
        retrofit.create(MyApiEndpointInterface.class)
                .getCategory(Utils.getDeviceId(this), Utils.getAppId(this), Utils.getLastTimeCategoriesLoaded(this))
                .enqueue(this);
        return true;
    }


    @Override
    public void onResponse(Call<Categories> call, Response<Categories> response) {
        if (response.isSuccessful()) {
            if (response.body().categories == null) {
                showErrorOccuredWindow();
            } else {

                showContent();
                HashMap<String, Integer> savedCounterMap = Utils.getCategoriesCounterMap(CategoryListActivity.this);
                final ArrayList<Categories.Category> categories = response.body().categories;

                for (Categories.Category category : categories) {
                    if (savedCounterMap.get(category.name) != null && savedCounterMap.get(category.name) != 0)
                        savedCounterMap.put(category.name, category.newVideos.count + savedCounterMap.get(category.name));
                    else
                        savedCounterMap.put(category.name, category.newVideos.count);
                }
                this.categories = categories;


                Utils.putLastTimeCategoriesLoaded(CategoryListActivity.this,
                        System.currentTimeMillis() / 1000);
                Utils.puttCategoriesCounterMap(CategoryListActivity.this,
                        savedCounterMap);


                recalculateCounters();

            }
        } else {
            showErrorOccuredWindow();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        recalculateCounters();
    }

    @Override
    public void onFailure(Call<Categories> call, Throwable t) {
        t.printStackTrace();
        showErrorOccuredWindow();

    }

    private void recalculateCounters(){
        HashMap<String, Integer> categoriesCounter = Utils.getCategoriesCounterMap(this);
        if (categoriesCounter == null || categories == null)
            return;
        Resources resources = getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, R.drawable.clouds);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint circlepaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(255, 255, 255));
        circlepaint.setColor(Color.rgb(255, 0, 0));
        // text size in pixels
        paint.setTextSize((int) (14 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        // draw text to the Canvas center
        Rect bounds = new Rect();

        if (categoriesCounter.get(categories.get(0).name) != 0 ){

            String counter = categoriesCounter.get(categories.get(0).name) + "";
            paint.getTextBounds(counter, 0, counter.length(), bounds);
            int x = (int) (bitmap.getWidth() * 0.80);
            int y = (int) (bitmap.getHeight() * 0.16);
            canvas.drawCircle(x, y, 14 * scale, circlepaint);
            canvas.drawText(counter, x - bounds.width()/2, y + bounds.height()/2, paint);

        }
        if (categoriesCounter.get(categories.get(1).name) != 0 ){

            String counter = categoriesCounter.get(categories.get(1).name) + "";
            paint.getTextBounds(counter, 0, counter.length(), bounds);
            int x = (int) (bitmap.getWidth() * 0.72);
            int y = (int) (bitmap.getHeight() * 0.36);
            canvas.drawCircle(x, y, 14 * scale, circlepaint);
            canvas.drawText(counter, x - bounds.width()/2, y + bounds.height()/2, paint);

        }
        if (categoriesCounter.get(categories.get(2).name) != 0 ){

            String counter = categoriesCounter.get(categories.get(2).name) + "";
            paint.getTextBounds(counter, 0, counter.length(), bounds);
            int x = (int) (bitmap.getWidth() * 0.42);
            int y = (int) (bitmap.getHeight() * 0.62);
            canvas.drawCircle(x, y, 14 * scale, circlepaint);
            canvas.drawText(counter, x - bounds.width()/2, y + bounds.height()/2, paint);

        }
        if (categoriesCounter.get(categories.get(3).name) != 0 ){

            String counter = categoriesCounter.get(categories.get(3).name) + "";
            paint.getTextBounds(counter, 0, counter.length(), bounds);
            int x = (int) (bitmap.getWidth() * 0.88);
            int y = (int) (bitmap.getHeight() * 0.64);
            canvas.drawCircle(x, y, 14 * scale, circlepaint);
            canvas.drawText(counter, x - bounds.width()/2, y + bounds.height()/2, paint);

        }

//        Glide.with(this)
//                .load(bitmap)
//                .transform(new FitCenter(this))
//                .into((ImageView) findViewById(R.id.clouds));
        ImageView clouds = (ImageView) findViewById(R.id.clouds);
        (clouds).setImageBitmap(bitmap);


//        paint.getTextBounds("12", 0, 2, bounds);
//        int x = (int) (bitmap.getWidth() * 0.80);
//        int y = (int) (bitmap.getHeight() * 0.14);
//        canvas.drawCircle(x, y, 14 * scale, circlepaint);
//        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);
//
//        paint.getTextBounds("23", 0, 2, bounds);
//        x = (int) (bitmap.getWidth() * 0.72);
//        y = (int) (bitmap.getHeight() * 0.36);
//        canvas.drawCircle(x, y, 14 * scale, circlepaint);
//        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);
//
//        paint.getTextBounds("34", 0, 2, bounds);
//        x = (int) (bitmap.getWidth() * 0.42);
//        y = (int) (bitmap.getHeight() * 0.64);
//        canvas.drawCircle(x, y, 14 * scale, circlepaint);
//        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);
//
//        paint.getTextBounds("45", 0, 2, bounds);
//        x = (int) (bitmap.getWidth() * 0.88);
//        y = (int) (bitmap.getHeight() * 0.64);
//        canvas.drawCircle(x, y, 14 * scale, circlepaint);
//        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);




    //        if (categoriesCounter.get(categories.get(1).name) != 0 ){
    //            cloud2counter.setText(categoriesCounter.get(categories.get(1).name) + "");
    //            cloud2counter.setVisibility(View.VISIBLE);
    //
    //        } else {
    //            cloud2counter.setVisibility(GONE);
    //        }
    //        if (categoriesCounter.get(categories.get(2).name) != 0 ){
    //            cloud3counter.setText(categoriesCounter.get(categories.get(2).name)+ "");
    //            cloud3counter.setVisibility(View.VISIBLE);
    //
    //        } else {
    //            cloud3counter.setVisibility(GONE);
    //        }
    //        if (categoriesCounter.get(categories.get(3).name) != 0 ){
    //            cloud4counter.setText(categoriesCounter.get(categories.get(3).name) + "");
    //            cloud4counter.setVisibility(View.VISIBLE);
    //
    //        } else {
    //            cloud4counter.setVisibility(GONE);
    //        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int evX = (int) event.getX();
        final int evY = (int) event.getY();

        if (event.getAction() == MotionEvent.ACTION_UP){
            int cloudIndex  = -1;
            int touchColor = getHotspotColor (R.id.clouds_map, evX, evY);
            if (closeMatch (Color.RED, touchColor, 25)) cloudIndex = 0;
            else if (closeMatch (Color.GREEN, touchColor, 25)) cloudIndex = 1;
            else if (closeMatch (Color.BLUE, touchColor, 25))cloudIndex = 2;
            else if (closeMatch (Color.CYAN, touchColor, 25))cloudIndex = 3;
            Log.d("***", touchColor + "");

            if (cloudIndex <0)
                return true;

            String tags = "";
            for (String s : categories.get(cloudIndex).tags){
                tags += s + ",";
            }
            tags = tags.substring(0, tags.length() - 1);
            Intent intent = new Intent(CategoryListActivity.this, CategoryActivity.class);
            intent.putExtra("tags", tags);
            intent.putExtra("name", categories.get(cloudIndex).name);
            startActivity(intent);
        }
        return true;
    }

    public int getHotspotColor (int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById (hotspotId);
        if (img == null) {
            android.util.Log.d ("ImageAreasActivity", "Hot spot image not found");
            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                android.util.Log.d ("ImageAreasActivity", "Hot spot bitmap was not created");
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }
    public boolean closeMatch (int color1, int color2, int tolerance) {
        if (Math.abs (Color.red (color1) - Color.red (color2)) > tolerance ) return false;
        if (Math.abs (Color.green (color1) - Color.green (color2)) > tolerance ) return false;
        if (Math.abs (Color.blue (color1) - Color.blue (color2)) > tolerance ) return false;
        return true;
    }


    @Override
    protected void onRestart() {
        super.onRestart();

    }

//    public void openCategory(View view) {
//        int cloudIndex = 0;
//        if (view.getId() == R.id.cloud1){
//            cloudIndex = 0;
//        } else if (view.getId() == R.id.cloud2){
//            cloudIndex = 1;
//        } else if (view.getId() == R.id.cloud3){
//            cloudIndex = 2;
//        } else if (view.getId() == R.id.cloud4){
//            cloudIndex = 3;
//        }
//
//        String tags = "";
//            for (String s : categories.get(cloudIndex).tags){
//                tags += s + ",";
//            }
//            tags = tags.substring(0, tags.length() - 1);
//            Intent intent = new Intent(CategoryListActivity.this, CategoryActivity.class);
//            intent.putExtra("tags", tags);
//            intent.putExtra("name", categories.get(cloudIndex).name);
//            startActivity(intent);
//    }

    public Bitmap drawTextToBitmap() {
        Resources resources = getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, R.drawable.clouds);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint circlepaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(255, 255, 255));
        circlepaint.setColor(Color.rgb(255, 0, 0));
        // text size in pixels
        paint.setTextSize((int) (14 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds("12", 0, 2, bounds);
        int x = (int) (bitmap.getWidth() * 0.80);
        int y = (int) (bitmap.getHeight() * 0.14);
        canvas.drawCircle(x, y, 14 * scale, circlepaint);
        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);

        paint.getTextBounds("23", 0, 2, bounds);
        x = (int) (bitmap.getWidth() * 0.72);
        y = (int) (bitmap.getHeight() * 0.36);
        canvas.drawCircle(x, y, 14 * scale, circlepaint);
        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);

        paint.getTextBounds("34", 0, 2, bounds);
        x = (int) (bitmap.getWidth() * 0.42);
        y = (int) (bitmap.getHeight() * 0.64);
        canvas.drawCircle(x, y, 14 * scale, circlepaint);
        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);

        paint.getTextBounds("45", 0, 2, bounds);
        x = (int) (bitmap.getWidth() * 0.88);
        y = (int) (bitmap.getHeight() * 0.64);
        canvas.drawCircle(x, y, 14 * scale, circlepaint);
        canvas.drawText("12", x - bounds.width()/2, y + bounds.height()/2, paint);

        return bitmap;
    }
}
