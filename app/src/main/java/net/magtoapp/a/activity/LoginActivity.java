package net.magtoapp.a.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import net.magtoapp.a.server.LoginResponse;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.server.UserRequest;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.server.StupidXmlConverterFactory;
import net.magtoapp.a.controller.Utils;

public class LoginActivity extends AppCompatActivity {

    private EditText emailView;
    private EditText passwordView;
    private EditText repeatPasswordView;
    private Button login;
    private Button toggle;
    private Button forgotPassword;

    private boolean isNewUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Glide.with(this)
                .load(R.drawable.logo)
                .into((ImageView) findViewById(R.id.logo));

        forgotPassword =(Button) findViewById(R.id.forgot_password);
        assert forgotPassword != null;
        forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        toggle =(Button) findViewById(R.id.toggle);
        assert toggle != null;
        toggle.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);



        emailView = (EditText) findViewById(R.id.email);
        passwordView = (EditText) findViewById(R.id.password);
        repeatPasswordView = (EditText) findViewById(R.id.repeat_password);
        toggle = (Button) findViewById(R.id.toggle);
        login = (Button) findViewById(R.id.login);
    }

    public void attemptLogin(View view) {

        final String email = emailView.getText().toString();
        String password = passwordView.getText().toString();
        String repeatPassword = repeatPasswordView.getText().toString();

        EditText focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(email)){
            focusView = emailView;
            emailView.setError(getString(R.string.need_to_fill_this_field));
            cancel = true;
        } else
        if (!email.contains("@")){
            focusView = emailView;
            emailView.setError(getString(R.string.wrong_email));
            cancel = true;
        } else
        if (TextUtils.isEmpty(password)){
            focusView = passwordView;
            passwordView.setError(getString(R.string.need_to_fill_this_field));
            cancel = true;
        }  else
        if (isNewUser && TextUtils.isEmpty(repeatPassword)){
            focusView = repeatPasswordView;
            repeatPasswordView.setError(getString(R.string.need_to_fill_this_field));
            cancel = true;
        } else
        if (isNewUser &&!repeatPassword.equals(password)){
            focusView = repeatPasswordView;
            repeatPasswordView.setError(getString(R.string.passwords_dont_match));
            cancel = true;
        }

        if (cancel){
            focusView.requestFocus();
        } else {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://media.magtoapp.net/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(new StupidXmlConverterFactory())
                    .build();

            MyApiEndpointInterface apiService = retrofit.create(MyApiEndpointInterface.class);
            if (isNewUser){
                apiService.register(new UserRequest(email, password)).enqueue(new Callback<UserRequest>() {
                    @Override
                    public void onResponse(Call<UserRequest> call, Response<UserRequest> response) {
                        if (!response.isSuccessful()) {
//                            ErrorClass error = ErrorUtils.parseRegistrationError(response, retrofit);
//                            if (error.user.errors.email != null)
//                                for (String errorText : error.user.errors.email) {
//                                    emailView.setError("Email " + errorText);
//                                }
//                            if (error.user.errors.password != null)
//                                for (String errorText : error.user.errors.password) {
//                                    passwordView.setError("Пароль " + errorText);
//                                }
                        }
                        try {
                            String token = response.body().user.user_token;
                            if (!TextUtils.isEmpty(token)) {
                                Utils.putToken(LoginActivity.this, token);
                                Utils.putEmail(LoginActivity.this, email);
                                Toast.makeText(LoginActivity.this, "Welcome!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(LoginActivity.this, CategoryListActivity.class));
                            } else {
                                Toast.makeText(LoginActivity.this, "Try again", Toast.LENGTH_LONG).show();
                            }
                        } catch (NullPointerException e) {
                            Toast.makeText(LoginActivity.this, "Try again", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserRequest> call, Throwable t) {

                        t.printStackTrace();
                        Toast.makeText(LoginActivity.this, "Error during information transferring", Toast.LENGTH_LONG).show();
                    }
                });
            } else {

                apiService.login(new UserRequest(email, password)).enqueue(new Callback<UserRequest>() {
                    @Override
                    public void onResponse(Call<UserRequest> call, Response<UserRequest> response) {
                        if (!response.isSuccessful()) {
//                            LoginErrorClass error = ErrorUtils.parseLoginError(response, retrofit);
//                            for (String errorText : error.user.errors) {
//                                Toast.makeText(LoginActivity.this, errorText, Toast.LENGTH_LONG).show();
//                            }
                        } else {
                            String token = response.body().user.user_token;
                            if (!TextUtils.isEmpty(token)) {
                                Utils.putToken(LoginActivity.this, token);
                                Utils.putEmail(LoginActivity.this, email);

                                Toast.makeText(LoginActivity.this, "Welcome!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(LoginActivity.this, CategoryListActivity.class));
                            } else {
                                Toast.makeText(LoginActivity.this, "Try again", Toast.LENGTH_LONG).show();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<UserRequest> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(LoginActivity.this, "Error during information transferring", Toast.LENGTH_LONG).show();
                    }

                });
            }
        }

    }

    public void toggleLoginRegistration(View view) {
        if (isNewUser){
            login.setText(R.string.login);
            toggle.setText(R.string.no_account);
            repeatPasswordView.setVisibility(View.GONE);
            isNewUser = false;
        } else {
            login.setText(R.string.register);
            toggle.setText(R.string.have_account);
            repeatPasswordView.setVisibility(View.VISIBLE);
            isNewUser = true;
        }
    }

    public void forgotPassword(View view) {
        final View v = View.inflate(LoginActivity.this, R.layout.email_edittext, null);
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setView(v)
                .setPositiveButton(R.string.change_password, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("https://media.magtoapp.net/")
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .addConverterFactory(new StupidXmlConverterFactory())
                                        .build();View.inflate(LoginActivity.this, R.layout.email_edittext, null);

                                MyApiEndpointInterface apiService = retrofit.create(MyApiEndpointInterface.class);
                                apiService.restore_password(((EditText) v.findViewById(R.id.email_et)).getText().toString())
                                .enqueue(new Callback<LoginResponse>() {
                                    @Override
                                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                        if (response.isSuccessful()){
                                            if (response.body() != null && response.body().result.equals("success")) {
                                                Toast.makeText(LoginActivity.this, "На Ваш email было отправлено письмо с инструкциями", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(LoginActivity.this, "Что-то пошло не такПопробуйте еще раз", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(LoginActivity.this, "Try again", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<LoginResponse> call, Throwable t) {

                                        Toast.makeText(LoginActivity.this, "Something bad happened", Toast.LENGTH_LONG).show();
                                    }

                                });
                            }
                        }

                ).

                    show();
                }

//    public static class ErrorUtils {
//
//
//        public static ErrorClass parseRegistrationError(Response<?> response, Retrofit retrofit) {
//            Converter<ResponseBody, ErrorClass> converter =
//                    retrofit.responseConverter(ErrorClass.class, new Annotation[0]);
//
//            ErrorClass error;
//
//            try {
//                error = converter.convert(response.errorBody());
//            } catch (IOException e) {
//                return new ErrorClass();
//            }
//
//            return error;
//        }
//        public static LoginErrorClass parseLoginError(Response<?> response, Retrofit retrofit) {
//            Converter<ResponseBody, LoginErrorClass> converter =
//                    retrofit.responseConverter(LoginErrorClass.class, new Annotation[0]);
//
//            LoginErrorClass error;
//
//            try {
//                error = converter.convert(response.errorBody());
//            } catch (IOException e) {
//                return new LoginErrorClass();
//            }
//
//            return error;
//        }
//    }

}
