package net.magtoapp.a.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.controller.Utils;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.recyclervieweffects.LargeTopItemOnScrollListener;
import net.magtoapp.a.server.MyApiEndpointInterface;
import trikita.log.Log;

public class CategoryActivity extends TemplateActivity implements Callback<ArrayList<VideoData>> {

    private RecyclerView recyclerView;
    private ArrayList<VideoData> dataset;
    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setMainContentView(R.layout.activity_category);
        setExtraBackButtonVisible(true);
        setWhileLineVisible(false);
        setPaddingContent(0, 0, 0, 0);
        if (getIntent().hasExtra("name"))
            setTitle(getIntent().getExtras().getString("name"));
        searchEnabled(true);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeLayout.setColorSchemeColors(Color.rgb(76, 200, 226));
        swipeLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.category_videos_recyclerview);
        if (Utils.isTablet(this)){
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        }
        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
//        SnapHelper snapHelperTop = new LinearSnapHelper();
//        snapHelperTop.attachToRecyclerView(recyclerView);
//        recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
        recyclerView.setAdapter(new VideoAdapter(this, new ArrayList<VideoData>()));
        setIsLocalSearch(true);


    }

    @Override
    public void performSearch() {
        if (dataset == null || dataset.size() == 0){
            showNothingFoundWindow();
            return;
        }
        List<VideoData> searchResults = new ArrayList<>();
        String searchPhrase = searchEditText.getText().toString();
        for (VideoData videoData : dataset){
            if (videoData.description.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.title.toLowerCase().contains(searchPhrase.toLowerCase())
                    || videoData.channel.title.toLowerCase().contains(searchPhrase.toLowerCase()))
                searchResults.add(videoData);
        }

        if (searchResults.size() != 0 ){

            ((RecyclerView) findViewById(R.id.search_result_videos_list)).setAdapter(new VideoAdapter(CategoryActivity.this, searchResults));
            showSearchResult();
        } else {
            showNothingFoundWindow();
        }

    }


    @Override
    public void onStart() {
        super.onStart();

        swipeLayout.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (recyclerView.getScrollY() == 0)
                    swipeLayout.setEnabled(true);
                else
                    swipeLayout.setEnabled(false);

            }
        });
        recyclerView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = recyclerView.getScrollY();
                if(scrollY == 0) swipeLayout.setEnabled(true);
                else swipeLayout.setEnabled(false);

            }
        });
    }

    @Override
    public boolean loadData() {
        retrofit.create(MyApiEndpointInterface.class).getVideosFromCategory(Utils.getDeviceId(this),
                Utils.getAppId(this),
                getIntent().getExtras().getString("tags")).enqueue(this);
        return true;
    }

    @Override
    public void onResponse(Call<ArrayList<VideoData>> call, Response<ArrayList<VideoData>> response) {
        if (response.isSuccessful()){
            if (response.body() == null || response.body().size() == 0){
                showNothingFoundWindow();
            } else {
                showContent();
            }


            if (getIntent().getExtras() != null){

                HashMap<String, Integer> counterDataset = Utils.getCategoriesCounterMap(CategoryActivity.this);
                counterDataset.put(getIntent().getExtras().getString("name"), 0);
                Utils.puttCategoriesCounterMap(CategoryActivity.this, counterDataset);
                ArrayList<VideoData> receivedItems = response.body();
                for (VideoData videoData : receivedItems){
                    videoData.channel.id = getIntent().getExtras().getString("tags", "") ;
                }
                dataset = receivedItems;

            } else {

                dataset = response.body();
            }

            ((VideoAdapter) recyclerView.getAdapter()).setDataset(dataset);
        } else {
            showErrorOccuredWindow();
        }

        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<ArrayList<VideoData>> call, Throwable t) {

        t.printStackTrace();
        showErrorOccuredWindow();

        swipeLayout.setRefreshing(false);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("***", "onActivityResult", requestCode, resultCode);
        if (requestCode == VideoAdapter.REQUEST_VISITED_VIDEOS && data != null){
            Log.d("***", "onActivityResult");
            ArrayList<String> visitedVideos = data.getStringArrayListExtra(VideoAdapter.KEY_VISITED_NEWS);
            for (VideoData videoData : dataset){
                for (String newsId : visitedVideos){
                    if (videoData.id.equals(newsId)){
                        videoData.views++;
                    }
                }
            }
            ((VideoAdapter) recyclerView.getAdapter()).setDataset(dataset);
        }
    }

  
}
