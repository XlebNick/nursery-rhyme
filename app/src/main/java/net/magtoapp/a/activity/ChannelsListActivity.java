package net.magtoapp.a.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.model.Channels;
import trikita.log.Log;

public class ChannelsListActivity extends TemplateActivity {

    RecyclerView recyclerView;

    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMainContentView(R.layout.activity_channels_list);
        setPaddingContent(0, 0, 0, 0);
        setExtraBackButtonVisible(true);
        setWhileLineVisible(false);
        setTitle(getString(R.string.authors_item_title));
        searchEnabled(true);

        type = ActivityType.NewsListActivity;

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeLayout.setColorSchemeColors(Color.rgb(76, 200, 226));
        swipeLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.channels_listview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new Adapter());
    }


    @Override
    public void onStart() {
        super.onStart();

        swipeLayout.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                swipeLayout.setEnabled(recyclerView.getScrollY() == 0);
            }
        });

    }


    @Override
    public boolean loadData() {
        MyApiEndpointInterface apiService = retrofit.create(MyApiEndpointInterface.class);
        Call<Channels> callVideos = apiService.getChannels(Utils.getAppId(this));
        callVideos.enqueue(new Callback<Channels>() {

            @Override
            public void onResponse(Call<Channels> call, Response<Channels> response) {

                ((Adapter) recyclerView.getAdapter()).setDataset(response.body().channels);
                showContent();

                Log.d(new Gson().toJson(response.body().channels));

                swipeLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<Channels> call, Throwable t) {
                t.printStackTrace();
                showErrorOccuredWindow();
                swipeLayout.setRefreshing(false);
            }

        });
        return true;
    }

    private class Adapter extends RecyclerView.Adapter<ChannelItemViewHolder>{

        List<Channels.Channel> dataset;

//        public Adapter(Context context, int resource, List<Channels.Channel> dataset) {
//            super(context, resource, dataset);
//            this.dataset = dataset;
//        }

//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            LayoutInflater inflater = (LayoutInflater) parent.getContext()
//                    .getSystemService(LAYOUT_INFLATER_SERVICE);
//            View view = inflater.inflate(R.layout.channels_listview_item, parent, false);
//            Glide.with(ChannelsListActivity.this)
//                    .load(dataset.get(position).thumbnails._90x90)
//                    .into(((ImageView) view.findViewById(R.id.channel_logo)));
//            ((TextView) view.findViewById(R.id.channel_name)).setText(dataset.get(position).title);
//            ((TextView) view.findViewById(R.id.channel_description)).setText(dataset.get(position).description);
//            view.setTag(dataset.get(position));
//            return view;
//        }

//        @Override
//        public int getCount() {
//            return dataset.size();
//        }

        @Override
        public ChannelItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.channels_listview_item, parent, false);

            return new ChannelItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ChannelItemViewHolder holder, int position) {
            Glide.with(ChannelsListActivity.this)
                    .load(dataset.get(position).thumbnails._90x90)
                    .into(holder.channelLogo);
            holder.item = dataset.get(position);
            holder.channelName.setText(holder.item.title);
            holder.channelDescription.setText(holder.item.description);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChannelsListActivity.this, ChannelActivity.class);
                    intent.putExtra("id", holder.item.id);
                    intent.putExtra("description", holder.item.description);
                    intent.putExtra("title", holder.item.title);
                    intent.putExtra("logo", holder.item.thumbnails._240x240);
//                intent.putExtra("channelTitle", ((Channels.Channel) view.getTag()).thumbnails._60x60);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataset == null ? 0 : dataset.size();
        }

        public void setDataset(List<Channels.Channel> dataset) {
            this.dataset = dataset;
            notifyDataSetChanged();
        }
    }

    private class ChannelItemViewHolder extends RecyclerView.ViewHolder {
        /*package*/ ImageView channelLogo;
        /*package*/ TextView channelName;
        /*package*/ TextView channelDescription;
        /*package*/ Channels.Channel item;

        /*package*/ ChannelItemViewHolder(View itemView) {
            super(itemView);
            channelLogo = (ImageView) itemView.findViewById(R.id.channel_logo);
            channelName = (TextView) itemView.findViewById(R.id.channel_name);
            channelDescription = (TextView) itemView.findViewById(R.id.channel_description);
        }
    }
}
