package net.magtoapp.a.server;

/**
 * Created by XlebNick for Carousel.
 */
public class CommentRequest {
    public String device_id;
    public String user_email;
    public String user_token;
    public String message;
    public String author_name;

    public CommentRequest(String device_id, String user_email, String user_token, String message, String author_name) {
        this.device_id = device_id;
        this.user_email = user_email;
        this.user_token = user_token;
        this.message = message;
        this.author_name = author_name;
    }

}
