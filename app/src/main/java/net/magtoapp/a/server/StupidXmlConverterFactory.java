package net.magtoapp.a.server;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import net.magtoapp.a.model.News;

/**
 * Created by XlebNick for Carousel.
 */

public class StupidXmlConverterFactory extends Converter.Factory {

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return new StupidXmlConverter(type);
    }


    private class StupidXmlConverter<T> implements Converter<ResponseBody, T>{

        private final Type type;

        public StupidXmlConverter(Type type) {
            this.type = type;
        }

        @Override
        public T convert(ResponseBody value) throws IOException {

            if (type.toString().equals("java.util.ArrayList<net.magtoapp.a.model.News>")){
                ArrayList<News> newsArrayList = new ArrayList<>();
                String[] strs = value.string().split("</item>");
                for (int i = 0; i < strs.length - 1; i++){
                    News news = new News();
                    news.creator = findTagValue(strs[i], "dc:creator");
                    news.date = findTagValue(strs[i], "pubDate");
                    news.link = findTagValue(strs[i], "magtoapp:permalink");
                    news.title = findTagValue(strs[i], "title");
                    newsArrayList.add(news);
                }
                return (T) newsArrayList;
            }
            return null;

        }
    }

    private String findTagValue(String str, String tag){
        return  str.substring(
                str.lastIndexOf("<" + tag + ">") + tag.length() + 2,
                str.lastIndexOf("</" + tag + ">"));

    }
}
