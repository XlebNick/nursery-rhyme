package net.magtoapp.a.server;

/**
 * Created on 22.10.2015.
 * All rights are reserved.
 * Please keep your hands out of my code without special permission.
 */

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import net.magtoapp.a.model.Categories;
import net.magtoapp.a.model.Channels;
import net.magtoapp.a.model.Comment;
import net.magtoapp.a.model.News;
import net.magtoapp.a.model.VideoCollection;
import net.magtoapp.a.model.VideoData;


public interface MyApiEndpointInterface {

    @GET("api/media/v1/content?type=video")
    Call<Channels> getChannels(@Query("app_id") String appId);

    @GET("api/media/v1/videos/latest")
    Call<ArrayList<VideoData>> getAllVideos(@Query("app_id") String appId);

    @GET("api/media/v1/video_channels/{channelId}/videos?&full_description=0")
    Call<ArrayList<VideoData>> getAllVideosFromChannel(@Path("channelId") String channelId,
                                                       @Query("app_id") String appId);

    @GET("api/media/v1/videos/{videoId}")
    Call<VideoData> getVideoInfo(@Path("videoId") String videoId,
                                 @Query("device_id") String deviceId,
                                 @Query("full_description") Integer fullDescription,
                                 @Query("app_id") String appId);

    @GET("api/media/v1/videos/favorites")
    Call<ArrayList<VideoData>> getStarred(@Query("device_id") String deviceId,
                                          @Query("user_email") String userEmail,
                                          @Query("user_token") String userToken,
                                          @Query("app_id") String appId);
    @GET("api/media/v1/news/feed.rss?end_pos=99999")
    Call<ArrayList<News>> getNews(@Query("start_pos") int startPosition,
                                  @Query("end_pos") int endPosition,
                                  @Query("app_id") String appId);

    @Headers({
            "Accept: application/json"})
    @GET("/auth/restore_password ")
    Call<LoginResponse> restore_password(@Query("user[user_email]") String user) ;

    @GET("api/media/v1/videos/search.json")
    Call<VideoCollection> searchVideos(@Query("query") String query,
                                       @Query("app_id") String appId);

    @GET("api/media/v1/news/search.rss?end_pos=99999")
    Call<ArrayList<News>> searchNews(@Query("query") String query,
                                     @Query("app_id") String appId);

    @GET("api/media/v1/videos/{videoID}/comments")
    Call<ArrayList<Comment>> getComments(@Path("videoID") String videoId,
                                         @Query("start_pos") int startPos,
                                         @Query("end_pos") int endPos,
                                         @Query("user_email") String userEmail,
                                         @Query("user_token") String userToken,
                                         @Query("device_id") String deviceId,
                                         @Query("app_id") String appId);

    @Headers({
            "Accept: application/json"})
    @GET("api/media/v1/comments/{commentID}/comments")
    Call<ArrayList<Comment>> getCommentsOfComment(@Path("commentID") String commentID,
                                                  @Query("start_pos") int startPos,
                                                  @Query("end_pos") int endPos,
                                                  @Query("user_email") String userEmail,
                                                  @Query("user_token") String userToken,
                                                  @Query("device_id") String deviceId,
                                                  @Query("app_id") String appId);

    @GET("api/media/v1/comments/{id}/like")
    Call<GeneralResponse> likeComment(@Path("id") String id,
                                      @Query("user_email") String userEmail,
                                      @Query("user_token") String userToken,
                                      @Query("device_id") String deviceId,
                                      @Query("app_id") String appId );

    @GET("api/media/v1/comments/{id}/dislike")
    Call<GeneralResponse> dislikeComment(@Path("id") String id,
                                         @Query("user_email") String userEmail,
                                         @Query("user_token") String userToken,
                                         @Query("device_id") String deviceId,
                                         @Query("app_id") String appId);



    @GET("api/media/v1/videos/categories")
    Call<Categories> getCategory(@Query("device_id") String deviceId,
                                 @Query("app_id") String appId,
                                 @Query("new_videos_since") long lastTimeCategoryLoaded);

    @GET("api/media/v1/videos/tagged")
    Call<ArrayList<VideoData>> getVideosFromCategory(@Query("device_id") String deviceId,
                                                     @Query("app_id") String appId,
                                                     @Query("tags") String tags);
    @GET("api/media/v1/video_channels/{channelId}")
    Call<Channels.Channel> getChannelInfo(@Path("channelId") String channelId,
                                          @Query("app_id") String appId);

    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/viewed")
    Call<String> setAsViewed(@Path("videoId") String videoId,
                             @Field("user_token") String userToken);

    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/like")
    Call<GeneralResponse> like(@Path("videoId") String videoId,
                               @Field("device_id") String deviceId,
                               @Field("app_id") String appId);
    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/dislike")
    Call<GeneralResponse> dislike(@Path("videoId") String videoId,
                                  @Field("device_id") String deviceId,
                                  @Field("user_token") String userToken,
                                  @Field("app_id") String appId);
    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/favorite")
    Call<GeneralResponse> star(@Path("videoId") String videoId,
                               @Field("device_id") String deviceId,
                               @Field("user_token") String userToken,
                               @Field("user_email") String userEmail,
                               @Field("app_id") String appId);

    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/notfavorite")
    Call<GeneralResponse> unstar(@Path("videoId") String videoId,
                               @Field("device_id") String deviceId,
                               @Field("user_token") String userToken,
                               @Field("user_email") String userEmail,
                                 @Field("app_id") String appId);

    @FormUrlEncoded
    @POST("api/media/v1/videos/{videoId}/unvote")
    Call<GeneralResponse> unvote(@Path("videoId") String videoId,
                                 @Field("device_id") String deviceId,
                                 @Field("user_token") String userToken,
                                 @Field("app_id") String appId);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"})
    @POST("/auth/login ")
    Call<UserRequest> login(@Body UserRequest credentials);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"})
    @POST("auth/register")
    Call<UserRequest> register(@Body UserRequest credentials);
    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"})
    @POST("api/media/v1/videos/{videoID}/comments")
    Call<GeneralResponse> addComment(@Path("videoID") String videoId,
                                     @Body CommentRequest request);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"})
    @POST("api/media/v1/comments/{commentID}/comments")
    Call<GeneralResponse> addCommentToComment(@Path("commentID") String commentId,
                                              @Body CommentRequest request);

    @FormUrlEncoded
    @POST("api/media/v1/comments/{videoId}/unvote")
    Call<GeneralResponse> unvoteComment(@Path("videoId") String videoId,
                                        @Field("device_id") String deviceId,
                                        @Field("user_token") String userToken,
                                        @Field("app_id") String appId);

    @GET("get_video_info")
    Call<String> getYoutubeManifest(@Query("video_id") String videoId );
}