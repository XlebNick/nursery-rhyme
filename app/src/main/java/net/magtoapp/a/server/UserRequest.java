package net.magtoapp.a.server;

/**
 * Created by XlebNick for Carousel.
 */


public class UserRequest {
    public Credentials user;

    public UserRequest(String email, String password){
        user = new Credentials(email, password);

    }

    public class Credentials{
        private final String password;
        private final String email;
        public String user_token;

        public Credentials(String email, String password){
            this.email = email;
            this.password = password;
        }
    }


}

