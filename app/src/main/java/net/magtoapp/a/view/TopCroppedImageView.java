package net.magtoapp.a.view;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.makeramen.roundedimageview.Corner;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by chris on 7/27/16.
 */
public class TopCroppedImageView extends RoundedImageView {
    private boolean hasCornersChanged = false;

    public TopCroppedImageView(Context context) {
        super(context);
        setup();
    }

    public TopCroppedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public TopCroppedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }


    private void setup() {
        setScaleType(ScaleType.MATRIX);
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        if (getDrawable() == null)
            return super.setFrame(l, t, r, b);

        Matrix matrix = getImageMatrix();

        float scale;
        int viewWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int viewHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int drawableWidth = getDrawable().getIntrinsicWidth();
        int drawableHeight = getDrawable().getIntrinsicHeight();
        //Get the scale
        if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
            scale = (float) viewHeight / (float) drawableHeight;
        } else {
            scale = (float) viewWidth / (float) drawableWidth;
        }

        //Define the rect to take image portion from
        RectF drawableRect = new RectF(0, drawableHeight - (viewHeight / scale), drawableWidth, drawableHeight);
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.FILL);

        setImageMatrix(matrix);

        if (!hasCornersChanged ){

            setCornerRadius(
                    getCornerRadius(Corner.TOP_LEFT) / scale,
                    getCornerRadius(Corner.TOP_RIGHT)/  scale,
                    getCornerRadius(Corner.BOTTOM_LEFT) / scale,
                    getCornerRadius(Corner.BOTTOM_RIGHT) / scale
            );
            hasCornersChanged = true;
        }

        return super.setFrame(l, t, r, b);
    }
}