package net.magtoapp.a.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.Locale;

import ru.magtoapp.xlebnick.a.R;

/**
 * Created by XlebNick for Carousel.
 */
public class CommentButton extends ViewSwitcher {

    public static final int STATE_DEACTIVATED = 1;
    public static final int STATE_ACTIVATED = 2;
    public static final int STATE_LOADING = 3;

    private boolean hasCounter;
    private int state;
    private Drawable normalButtonDrawable;
    private Drawable activatedButtonDrawable;

    private TextView counter;
    private ImageView image;
    private View content;
    private View progressBar;

    public CommentButton(Context context){
        super(context);
    }


    public CommentButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }
//
//    public CommentButton(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init(context, attrs);
//    }


    private void init(Context context, AttributeSet attrs) {
        setMeasureAllChildren(true);

        inflate(context, R.layout.comment_button, this);
        counter = (TextView) findViewById(R.id.counter);
        image = (ImageView) findViewById(R.id.image);
        content = findViewById(R.id.content);
        progressBar = findViewById(R.id.progress_bar);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CommentButton, 0, 0);
        try {
            hasCounter = ta.getBoolean(R.styleable.CommentButton_hasCounter, false);
            try {

                int activatedButtonDrawableId = ta.getResourceId(R.styleable.CommentButton_iconActivated, 0);
                int normalButtonDrawableId = ta.getResourceId(R.styleable.CommentButton_iconNormal, 0);

                activatedButtonDrawable = getResources().getDrawable(activatedButtonDrawableId);
                normalButtonDrawable = getResources().getDrawable(normalButtonDrawableId);
            } catch (Exception e){
                e.printStackTrace();
            }

            progressBar.setVisibility(GONE);
            content.setVisibility(VISIBLE);
            image.setImageDrawable(normalButtonDrawable);
            state = STATE_DEACTIVATED;
        } finally {
            ta.recycle();
        }
    }

    public void setState(int state, int counter){
        switch (state){
            case STATE_DEACTIVATED:
                image.setImageDrawable(normalButtonDrawable);
                this.counter.setText(String.format(Locale.getDefault(), "%d", counter));
                if (getNextView().getId() == R.id.content){
                    showNext();
                }
//                content.setVisibility(VISIBLE);
//                progressBar.setVisibility(GONE);
                this.state = state;
                break;
            case STATE_ACTIVATED:
                image.setImageDrawable(activatedButtonDrawable);
                this.counter.setText(String.format(Locale.getDefault(), "%d", counter));

                if (getNextView().getId() == R.id.content){
                    showNext();
                }
//                content.setVisibility(VISIBLE);
//                progressBar.setVisibility(GONE);
                this.state = state;
                break;
            case STATE_LOADING:

                if (getNextView().getId() == R.id.progress_bar){
                    showNext();
                }
//                content.setVisibility(GONE);
//                progressBar.setVisibility(VISIBLE);
                this.state = state;
                break;
        }
    }

    public int getState() {
        return state;
    }

    public int getCounter(){
        return Integer.parseInt(counter.getText().toString());
    }
    public void setCounter(int counter){
        this.counter.setText(String.format(Locale.getDefault(), "%d", counter));
    }
}
