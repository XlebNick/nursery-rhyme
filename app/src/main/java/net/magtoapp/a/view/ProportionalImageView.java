package net.magtoapp.a.view;

        import android.content.Context;
        import android.util.AttributeSet;
        import android.util.Log;

/**
 * Created by Navneet Boghani on 7/7/16.
 */
public class ProportionalImageView extends android.support.v7.widget.AppCompatImageView {

    public ProportionalImageView(Context context) {
        super(context);
    }

    public ProportionalImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProportionalImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int ih = MeasureSpec.getSize(heightMeasureSpec);//height of imageView
        int iw = MeasureSpec.getSize(widthMeasureSpec);//width of imageView
        int iH = getDrawable().getIntrinsicHeight();//original height of underlying image
        int iW = getDrawable().getIntrinsicWidth();//original width of underlying image

        Log.d("***", ih + " " + iw + " " + iH + " " + iW);
        if (iw/ih > iW/iH){
            //stretch so height fits parent
            iW = (ih - 50) * iW/ iH;
            iH = ih - 50;

        } else {
            iW = iw;
            iH = iw * iH / iW;
        }

        Log.d("***", ih + " later " + iw + " " + iH + " " + iW);


        setMeasuredDimension(iW, ih);

        getLayoutParams().width = iW;
        getLayoutParams().height = ih;

    }
}