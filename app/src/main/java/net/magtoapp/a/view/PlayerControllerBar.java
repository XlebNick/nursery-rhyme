package net.magtoapp.a.view;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.google.android.exoplayer.ExoPlayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import ru.magtoapp.xlebnick.a.R;

/**
 * Created by XlebNick for Carousel.
 */
public class PlayerControllerBar extends FrameLayout implements SeekBar.OnSeekBarChangeListener {

    private TextView tvTotalDuraton;
    private TextView tvCurrentPosition;
    private AppCompatSeekBar seekBar;
    private ImageView btnPlay;
    private long duration;
    private long position;
    private CountDownTimer timer;
    private EMVideoView emVideoView;
    private Timer tickTimer;
    private boolean isPlaying;
    private ExoPlayer.Listener listener;

    public PlayerControllerBar(Context context) {
        super(context);
        init();
    }

    public PlayerControllerBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlayerControllerBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.player_controller_bar, this);
        seekBar = (AppCompatSeekBar) findViewById(R.id.seekbar);
        btnPlay = (ImageView) findViewById(R.id.button_play);
        tvCurrentPosition = (TextView) findViewById(R.id.tv_current_position);
        tvTotalDuraton = (TextView) findViewById(R.id.tv_total_duration);

        btnPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emVideoView == null)
                    return;
                if (emVideoView.isPlaying()) {

                    pause();
                } else {
                    play();
                }
            }
        });
        seekBar.setOnSeekBarChangeListener(this);
    }

    public boolean isShowing() {
        return getVisibility() == VISIBLE;
    }

    public void hide() {
        Log.d("thisistitle", "hide");
        setVisibility(INVISIBLE);
    }

    public void show(int i) {
        setVisibility(VISIBLE);
        timer = new CountDownTimer(i*1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                hide();
            }
        };
    }

    public void setMediaPlayer(EMVideoView emVideoView) {
        this.emVideoView = emVideoView;

        duration = emVideoView.getDuration();
        position = emVideoView.getCurrentPosition();
        tvCurrentPosition.setText(convertMillisToTime(position));
        tvTotalDuraton.setText(convertMillisToTime(duration));
        startTickTimer();
    }



    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser){

            int newPosition = (int) (duration * progress / 1000f);
            emVideoView.seekTo(newPosition);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void play(){

        isPlaying = true;
        emVideoView.start();
        btnPlay.setImageResource(android.R.drawable.ic_media_pause);
        startTickTimer();
        if (listener != null){
            listener.onPlayerStateChanged(true, 0);
            listener.onPlayWhenReadyCommitted();
        }

    }

    public void pause(){
        isPlaying = false;

        emVideoView.pause();
        btnPlay.setImageResource(android.R.drawable.ic_media_play);
        tickTimer.cancel();
        tickTimer.purge();
        if (listener != null){
            listener.onPlayerStateChanged(false, 0);
            listener.onPlayWhenReadyCommitted();
        }
    }

    public void setPlayWhenReadyListener(ExoPlayer.Listener listener){
        this.listener = listener;

    }

    private void startTickTimer(){
        tickTimer = new Timer();
        tickTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isPlaying != emVideoView.isPlaying()){
                            if(emVideoView.isPlaying()){
                                play();
                            } else {
                                pause();
                            }
                        }
                        duration = emVideoView.getDuration();
                        position = emVideoView.getCurrentPosition();
                        tvCurrentPosition.setText(convertMillisToTime(position));
                        tvTotalDuraton.setText(convertMillisToTime(duration));
                        if(duration!= 0)
                            seekBar.setProgress((int) (1000 * position / duration));
                        seekBar.setSecondaryProgress(emVideoView.getBufferPercentage()*10);
                    }
                });

            }

        }, 0, 200);
    }

    private String convertMillisToTime(long millis){
        Date date = new Date(millis);
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        return sdf.format(date);
    }
}
