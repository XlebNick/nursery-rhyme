package net.magtoapp.a.view;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class BorderedTextView extends TextView {

    public BorderedTextView(Context context) {
        super(context);
    }
    public BorderedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public BorderedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        final ColorStateList textColor = getTextColors();

        TextPaint paint = this.getPaint();

        paint.setStyle(Style.STROKE);
        paint.setStrokeJoin(Join.ROUND);
        paint.setStrokeMiter(10);
        this.setTextColor(Color.BLACK);
        paint.setStrokeWidth(5);

        super.onDraw(canvas);
        paint.setStyle(Style.FILL);

        setTextColor(textColor);
        super.onDraw(canvas);
    }
}
