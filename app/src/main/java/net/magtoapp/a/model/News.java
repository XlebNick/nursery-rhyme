package net.magtoapp.a.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by XlebNick for carousel-mainpage-test.
 */
@Root(name = "item")
public class News {
    @Element(name = "dv:creator")
    public String creator;

    @Element(name = "magtoapp:permalink")
    public String link;

    @Element(name = "title")
    public  String title;

    @Element(name = "pubDate")
    public String date;

    public boolean isVisited = false;
}
