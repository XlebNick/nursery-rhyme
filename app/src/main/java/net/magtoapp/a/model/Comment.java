package net.magtoapp.a.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by XlebNick for Carousel.
 */
public class Comment {

    public String id;
    public String message;

    @SerializedName("created_at")
    public Date publishedAt;
    @SerializedName("likes_count")
    public int likes;
    @SerializedName("dislikes_count")
    public int dislikes;
    @SerializedName("comments_count")
    public int commentsCount;

    public String voted;

    public String parentId = "";
    public int nestingLevel = 0;

    public boolean isChildrenLoaded = false;

    public CommentAuthor author;

    public class CommentAuthor {
        public String id;
        public String name;
        public CommentAuthorAvatar avatar;
    }

    public class CommentAuthorAvatar {
        public int code;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Comment){
            return id.equals(((Comment) obj).id);
        }
        return super.equals(obj);
    }
}
