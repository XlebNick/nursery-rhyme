package net.magtoapp.a.model;

import java.util.ArrayList;

/**
 * Created by XlebNick for Carousel.
 */
public class VideoCollection {
    public Videos videos;

    public class Videos{
        public ArrayList<VideoData> collection;
    }
}
