package net.magtoapp.a.model;

import java.io.Serializable;

/**
 * Created by XlebNick for Carousel.
 */
public class HistoryItem implements Serializable{

    public String channelId;
    public String title;
    public String description;
    public String channel;
    public int views;
    public String thumbnails;
    public String publishedAt;
    public String url;
    public long timeofWatching;
    public int like;
    public int dislike;
}
