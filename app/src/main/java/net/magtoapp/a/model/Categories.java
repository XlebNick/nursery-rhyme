package net.magtoapp.a.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by XlebNick for Carousel.
 */
public class Categories {

    public ArrayList<Category> categories;

    public class Category {

        public String name;
        public String logo;
        public ArrayList<String> tags;
        @SerializedName("new_videos")
        public NewVideosCount newVideos;

    }

    public class NewVideosCount{
        public int count;
    }
}
