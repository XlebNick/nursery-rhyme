
package net.magtoapp.a.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Channels {

    public List<Channel> channels = new ArrayList<>();

    public class Channel {

        public String id;
        public String url;
        public String title;
        public String description;
        public Thumbnails thumbnails;
    }

    public class Thumbnails {

        @SerializedName(value="30x30")
        public String _30x30;
        @SerializedName(value="51x51")
        public String _51x51;
        @SerializedName(value="60x60")
        public String _60x60;
        @SerializedName(value="75x75")
        public String _75x75;
        @SerializedName(value="90x90")
        public String _90x90;
        @SerializedName(value="102x102")
        public String _102x102;
        @SerializedName(value="120x120")
        public String _120x120;
        @SerializedName(value="150x150")
        public String _150x150;
        @SerializedName(value="180x180")
        public String _180x180;
        @SerializedName(value="240x240")
        public String _240x240;

    }
}

