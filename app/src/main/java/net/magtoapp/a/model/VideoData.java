package net.magtoapp.a.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoData {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("published_at")
    @Expose
    public String publishedAt;
    @SerializedName("views")
    @Expose
    public Integer views;
    @SerializedName("likes")
    @Expose
    public Integer likes;
    @SerializedName("dislikes")
    @Expose
    public Integer dislikes;
    @SerializedName("thumbnails")
    @Expose
    public Thumbnails thumbnails;
    @SerializedName("voted")
    @Expose
    public String voted;
    public boolean isStarred;
    public boolean isExpanded = false;

    public class Thumbnails {

        @SerializedName("1280x720")
        @Expose
        public String _1280x720;
        @SerializedName("220x124")
        @Expose
        public String _220x124;
        @SerializedName("275x155")
        @Expose
        public String _275x155;
        @SerializedName("314x176")
        @Expose
        public String _314x176;
        @SerializedName("440x248")
        @Expose
        public String _440x248;
        @SerializedName("528x297")
        @Expose
        public String _528x297;
        @SerializedName("568x320")
        @Expose
        public String _568x320;

    }
    public Channel channel;
    public long timeOfWatching;

    public VideoData(){
        id ="";
        url ="";
        title="";
        description="";
        publishedAt="";
        views=0;
        thumbnails = new Thumbnails();
        thumbnails._440x248 = "";
        thumbnails._1280x720 = "";
        channel = new Channel();
        channel.id = "";
        channel.title = "";
        channel.humbnails = new Humbnails();
        channel.humbnails._60x60 = "";
    }

    public class Humbnails {
        @SerializedName("30x30")
        @Expose
        public String _30x30;
        @SerializedName("51x51")
        @Expose
        public String _51x51;
        @SerializedName("60x60")
        @Expose
        public String _60x60;
        @SerializedName("75x75")
        @Expose
        public String _75x75;
        @SerializedName("90x90")
        @Expose
        public String _90x90;
        @SerializedName("102x102")
        @Expose
        public String _102x102;
        @SerializedName("120x120")
        @Expose
        public String _120x120;
        @SerializedName("150x150")
        @Expose
        public String _150x150;
        @SerializedName("180x180")
        @Expose
        public String _180x180;
        @SerializedName("240x240")
        @Expose
        public String _240x240;

    }

    public class Channel {

        public Humbnails humbnails;
        public String id;
        public String title;

    }

}