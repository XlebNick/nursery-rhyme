package net.magtoapp.a.recyclervieweffects;

import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by XlebNick for Carousel.
 */
public class LargeTopItemOnScrollListener extends RecyclerView.OnScrollListener {
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 30)
           ((InputMethodManager)recyclerView.getContext().getSystemService(INPUT_METHOD_SERVICE))
                   .hideSoftInputFromWindow(recyclerView.getWindowToken(), 0);
    }

    //    private int startOffset;
//    private int endOffset;

//    @Override
//    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//        if (RecyclerView.SCROLL_STATE_IDLE == newState) {
//            final int scrollDistance = getScrollDistanceOfColumnClosestToLeft(recyclerView);
//            if (scrollDistance != 0) {
//                recyclerView.scrollBy(scrollDistance, 0);
//            }
//        }
//    }
//    private int getScrollDistanceOfColumnClosestToLeft(final RecyclerView recyclerView) {
//        final LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
//        final RecyclerView.ViewHolder firstVisibleColumnViewHolder = recyclerView.findViewHolderForAdapterPosition(manager.findFirstVisibleItemPosition());
//        if (firstVisibleColumnViewHolder == null) {
//            return 0;
//        }
//        final int columnHeight = firstVisibleColumnViewHolder.itemView.getMeasuredHeight();
//        final int top = firstVisibleColumnViewHolder.itemView.getTop();
//        final int absoluteTop = Math.abs(top);
//        return absoluteTop <= (columnHeight / 2) ? top : columnHeight - absoluteTop;
//    }
//
//    public void setOffset(int startOffset, int endOffset){
//        this.startOffset = startOffset;
//        this.endOffset = endOffset;
//    }
//
//
//    @Override
//    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//        if (recyclerView.getChildCount() > 2){
//
//            int firstVideoIndex = 0;
//            while (!(recyclerView.getChildViewHolder(recyclerView.getChildAt(firstVideoIndex)) instanceof VideoAdapter.VideoViewHolder)){
//                firstVideoIndex++;
//            }
//            Log.d(firstVideoIndex);
//            float f =((float) recyclerView.getChildAt(1 + firstVideoIndex).getTop())/ ((float) recyclerView.getChildAt(firstVideoIndex).getHeight());
//            while (f < 0)
//                f ++;
//            if (f >= 0 && f <= 1){
//                f = 1 - f;
//
//                try{
//                    ((VideoAdapter.VideoViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(1))).setExpandedExtent(f);
//                } catch (ClassCastException ignored){
//                    ignored.printStackTrace();
//                }
//
//                try{
//                    ((VideoAdapter.VideoViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(firstVideoIndex))).setExpandedExtent(1);
//                } catch (ClassCastException ignored){
//                    ignored.printStackTrace();
//                }
//                for (int i = 2 ; i < 3; i ++){
//
//                    if (recyclerView.getChildAt(i) != null){
//                        try{
//                            ((VideoAdapter.VideoViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i))).setExpandedExtent(0);
//                        } catch (ClassCastException ignored){
//                            ignored.printStackTrace();
//                        }
//                    }
//                }
//
//
//            }
//        }
//        super.onScrolled(recyclerView, dx, dy);
//    }




}
