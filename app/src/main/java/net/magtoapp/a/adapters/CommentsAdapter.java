package net.magtoapp.a.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import net.magtoapp.a.view.CommentButton;
import net.magtoapp.a.server.GeneralResponse;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.server.StupidXmlConverterFactory;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.Comment;
import net.magtoapp.a.activity.PlayerActivity;

/**
 * Created by XlebNick for Carousel.
 */
public class CommentsAdapter extends RecyclerView.Adapter implements Callback<ArrayList<Comment>> {
    private static final int TYPE_COMMENT_ITEM = 1;
    private static final int TYPE_ADD_COMMENT_ITEM = 0;
    private static final int TYPE_DETAILS = 2;
    private static final int TYPE_ADD_HEADER = 3;

    private static final String EMPTY = "empty";
    private static final String LIKED = "liked";
    private static final String DISLIKED = "disliked";

    public List<Comment> comments;
    private final Context context;

    private ArrayList<String> dislikedCommentsId = new ArrayList<>();
    private ArrayList<String> likedCommentsId = new ArrayList<>();

    private boolean isCommentLoading = false;
    private String loadingCommentId = "";
    private MyApiEndpointInterface apiService;

    private Call<GeneralResponse> currentCall;
    private boolean isLikeCancelled = false;

    public CommentsAdapter(Context context, List<Comment> objects) {
        super();

        this.context = context;
        this.comments = objects;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://media.magtoapp.net/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("dd-MM-yyyy hh:mm:ss").create()))
                .addConverterFactory(new StupidXmlConverterFactory())
                .build();

        apiService = retrofit.create(MyApiEndpointInterface.class);
    }

    //TODO!!!! Tagged activity element As in history

    @Override
    public int getItemViewType(int position) {

//        if (position == 0){
//            return TYPE_ADD_COMMENT_ITEM;
//        } else if (position == getItemCount()-1){
//            return TYPE_ADD_HEADER;
//        } else {
//        }
        return TYPE_COMMENT_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (viewType == TYPE_COMMENT_ITEM){
            view = inflater.inflate(R.layout.comment_item, parent, false);
            return new CommentViewHolder(view);
        } else if (viewType == TYPE_ADD_HEADER){
            return new RecyclerView.ViewHolder(inflater.inflate(R.layout.empty_item, parent,false)) {
            };
        } else{
            view = inflater.inflate(R.layout.add_comment, parent, false);
            return new AddCommentViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
//        if (position == 0){
//            ((AddCommentViewHolder) holder).childrenCount.setText(String.format(Locale.getDefault(),
//                    "Комментариев: %d", comments.size()));
//        } else if (position == getItemCount() - 1) {
//            return;
//        } else{

            final CommentViewHolder commentHolder = (CommentViewHolder) holder;
            final Comment comment = comments.get(position );
            if (comment == null){
                commentHolder.progressBar.setVisibility(View.VISIBLE);
                commentHolder.commentBody.setVisibility(View.GONE);
                commentHolder.avatarView.setVisibility(View.GONE);
                return;
            } else {
                commentHolder.progressBar.setVisibility(View.GONE);
                commentHolder.commentBody.setVisibility(View.VISIBLE);
                commentHolder.avatarView.setVisibility(View.VISIBLE);
            }


            commentHolder.answerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((PlayerActivity)context).showAddCommentView(comment.id);
                }
            });
            commentHolder.like.setCounter(comment.likes);
            commentHolder.dislike.setCounter(comment.dislikes);
            if (comment.voted != null){

                if (comment.voted.equals(LIKED)){
                    commentHolder.like.setState(CommentButton.STATE_ACTIVATED, getLikes(comment));
                    commentHolder.dislike.setState(CommentButton.STATE_DEACTIVATED, getDislikes(comment));
                } else if (comment.voted.equals(DISLIKED)){
                    commentHolder.dislike.setState(CommentButton.STATE_ACTIVATED, getDislikes(comment));
                    commentHolder.like.setState(CommentButton.STATE_DEACTIVATED, getLikes(comment));
                } else {
                    commentHolder.dislike.setState(CommentButton.STATE_DEACTIVATED, getDislikes(comment));
                    commentHolder.like.setState(CommentButton.STATE_DEACTIVATED, getLikes(comment));

                }
            }
            commentHolder.messageView.setText(comment.message);
            commentHolder.nicknameView.setText((comment.author != null && comment.author.name != null ) ? comment.author.name : "");
            commentHolder.publishedAtView.setText(Utils.generateStringFromDate(comment.publishedAt));

            commentHolder.expandButton.setVisibility(View.VISIBLE);
            commentHolder.expandButtonProgressBar.setVisibility(View.GONE);

            if (comment.commentsCount == 0){
                commentHolder.showMore.setVisibility(View.GONE);
            } else {
                commentHolder.showMore.setVisibility(View.VISIBLE);
            }

            if (comment.isChildrenLoaded){
                ((CommentViewHolder)holder).childrenCountView
                        .setText("Hide answers (" + comment.commentsCount + ")");
                commentHolder.expandButton.setRotation(180);
                commentHolder.showMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isCommentLoading)
                            return;
                        ArrayList<Comment> toDelete = new ArrayList<>();

                        for (Comment aComment : comments){
                            if (comment.id.equals(aComment.parentId)){
                                toDelete.add(aComment);
                            }
                        }

                        comments.removeAll(toDelete);
                        comment.isChildrenLoaded = false;
                        notifyItemRangeRemoved(position + 1, toDelete.size());
                        notifyItemRangeChanged(position, comments.size() - position);
                        notifyDataSetChanged();
                    }
                });
            } else {
                commentHolder.childrenCountView.setText("Show answers (" + comment.commentsCount + ")");
                commentHolder.expandButton.setRotation(0);
                commentHolder.showMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingCommentId = comment.id;
                        apiService.getCommentsOfComment(comment.id,
                                1,
                                999,
                                Utils.getEmail(context),
                                Utils.getToken(context),
                                Utils.getDeviceId(context),
                                Utils.getAppId(context))
                        .enqueue(CommentsAdapter.this);
                        commentHolder.expandButton.setVisibility(View.GONE);
                        commentHolder.expandButtonProgressBar.setVisibility(View.VISIBLE);
                    }
                });
            }

//            holder.itemView.setPadding((int) (22 * comment.nestingLevel * context.getResources().getDisplayMetrics().density), 0, 0, 0);

            commentHolder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentCall != null){
                        currentCall.cancel();
                        isLikeCancelled = true;
                    }
                    performLikeOrDislike(true, comment, commentHolder.like);
                    commentHolder.dislike.setState(CommentButton.STATE_DEACTIVATED, getDislikes(comment));

                }
            });
            commentHolder.dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentCall != null){
                        currentCall.cancel();
                        isLikeCancelled = true;
                    }
                    performLikeOrDislike(false, comment, commentHolder.dislike);
                    commentHolder.like.setState(CommentButton.STATE_DEACTIVATED, getLikes(comment));

                }
            });
//        }

    }

    private int getLikes(Comment comment){

        if (comment.voted == null){
            return comment.likes;
        }
        return comment.likes + (comment.voted.equals(LIKED) ? 1 : 0);
    }


    private int getDislikes(Comment comment){
        if (comment.voted == null){
            return comment.dislikes;
        }
        return comment.dislikes + (comment.voted.equals(DISLIKED) ? 1 : 0);
    }

    private void performLikeOrDislike(final boolean isLike, final Comment comment, final CommentButton commentButton){

        final String previousState = comment.voted;

        if (comment.voted.equals(isLike ? LIKED : DISLIKED)){
            commentButton.setState(CommentButton.STATE_LOADING, isLike ? getLikes(comment): getDislikes(comment));
            currentCall = apiService.unvoteComment(comment.id, Utils.getDeviceId(context),
                    Utils.getToken(context),
                    Utils.getAppId(context)
            );
            currentCall.enqueue(new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    if (!response.isSuccessful()){
                        comment.voted = isLike ? LIKED : DISLIKED;
                        commentButton.setState(CommentButton.STATE_ACTIVATED, isLike ? getLikes(comment): getDislikes(comment));
                        Toast.makeText(context, "Error during " + (isLike ? "" : "dis") + "like cancel", Toast.LENGTH_SHORT).show();
                    } else {
                        comment.voted = EMPTY;
                        commentButton.setState(CommentButton.STATE_DEACTIVATED, isLike ? getLikes(comment): getDislikes(comment));
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    t.printStackTrace();
                    comment.voted = previousState;
                    if (isLikeCancelled){
                        isLikeCancelled = false;
                    } else {
                        Toast.makeText(context, "Error during " + (isLike ? "" : "dis") + "like cancel", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            comment.voted = isLike ? LIKED : DISLIKED;
            commentButton.setState(CommentButton.STATE_LOADING, isLike ? getLikes(comment): getDislikes(comment));
            Callback<GeneralResponse> callback = new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    if (!response.isSuccessful()){
                        comment.voted = previousState;
                        commentButton.setState(CommentButton.STATE_DEACTIVATED, isLike ? getLikes(comment): getDislikes(comment));
                        Toast.makeText(context, "Error during " + (isLike ? "" : "dis") + "liking", Toast.LENGTH_SHORT).show();
                    } else {
                        comment.voted = isLike ? LIKED : DISLIKED;
                        commentButton.setState(CommentButton.STATE_ACTIVATED, isLike ? getLikes(comment): getDislikes(comment));
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    t.printStackTrace();
                    comment.voted = previousState;
                    commentButton.setState(CommentButton.STATE_DEACTIVATED, isLike ? getLikes(comment): getDislikes(comment));
                    if (isLikeCancelled){
                        isLikeCancelled = false;
                    } else {
                        Toast.makeText(context, "Error during " + (isLike ? "" : "dis") + "liking", Toast.LENGTH_SHORT).show();
                    }
                }
            };
            if (isLike){
                currentCall = apiService.likeComment(comment.id, Utils.getEmail(context),
                        Utils.getToken(context), Utils.getDeviceId(context),
                        Utils.getAppId(context));
            } else {
                currentCall = apiService.dislikeComment(comment.id, Utils.getEmail(context),
                        Utils.getToken(context), Utils.getDeviceId(context),
                        Utils.getAppId(context));
            }
            currentCall.enqueue(callback);
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
//            holder.view = null;
    }

    @Override
    public int getItemCount() {
        return comments.size() ;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setDataset(ArrayList<Comment> dataset) {
        this.comments = dataset;
        notifyDataSetChanged();
        notifyItemRangeChanged(0, dataset.size() );
    }

    @Override
    public void onResponse(Call<ArrayList<Comment>> call, Response<ArrayList<Comment>> response) {
        if (response.isSuccessful()) {

            int index = 0;
            for (int i = 0; i < comments.size(); i++) {
                if (comments.get(i).id.equals(loadingCommentId)){
                    index = i;
                    comments.get(i).isChildrenLoaded = true;
                    break;
                }
            }
            ArrayList<Comment> childrenComments = response.body();
            for (Comment comment : childrenComments){
                comment.nestingLevel = 1;
                comment.parentId = loadingCommentId;

                if (comment.voted == null){
                    comment.voted = EMPTY;
                }
                if (comment.voted.equals(LIKED)){
                    comment.likes--;
                }
                if (comment.voted.equals(DISLIKED)){
                    comment.dislikes--;
                }
            }

            comments.addAll(index + 1, childrenComments);
            notifyItemRangeChanged(index, comments.size() - index);
            notifyItemRangeChanged(0, comments.size());
            notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(Call<ArrayList<Comment>> call, Throwable t) {

    }

    private class CommentViewHolder extends RecyclerView.ViewHolder{

        LinearLayout showMore;
        TextView childrenCountView;
        TextView messageView;
        TextView nicknameView;
        TextView publishedAtView;
        CommentButton like;
        CommentButton dislike;
        View commentBody;
        View progressBar;
        View answerButton;
        ImageView avatarView;
        ImageView expandButton;
        View expandButtonProgressBar;

        public CommentViewHolder(View itemView) {
            super(itemView);
            commentBody = itemView.findViewById(R.id.comment_body);
            progressBar = itemView.findViewById(R.id.comment_progressbar);
            answerButton =  itemView.findViewById(R.id.btn_comment);
            childrenCountView = (TextView) itemView.findViewById(R.id.comment_children_count);
            showMore = (LinearLayout) itemView.findViewById(R.id.show_choldren);
            messageView = (TextView) itemView.findViewById(R.id.message);
            nicknameView = (TextView) itemView.findViewById(R.id.nickname);
            like = (CommentButton) itemView.findViewById(R.id.like);
            dislike = (CommentButton) itemView.findViewById(R.id.dislike);
            publishedAtView = (TextView) itemView.findViewById(R.id.tv_date);
            avatarView = (ImageView) itemView.findViewById(R.id.avatar);
            expandButton = (ImageView) itemView.findViewById(R.id.comment_expand_btn);
            expandButtonProgressBar = itemView.findViewById(R.id.comment_expand_btn_progress_bar);

        }
    }


    private class AddCommentViewHolder extends RecyclerView.ViewHolder{
        TextView childrenCount;
        View addCommentButton;
        public AddCommentViewHolder(View itemView) {
            super(itemView);
            childrenCount = (TextView) itemView.findViewById(R.id.comments_count);
            addCommentButton = itemView.findViewById(R.id.add_comment_kinda_button);
            addCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((PlayerActivity)context).showAddCommentView();
                }
            });
        }
    }
}

