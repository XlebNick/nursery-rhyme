package net.magtoapp.a.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.News;
import net.magtoapp.a.activity.NewsActivity;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private List<News> dataset;
    private Context context;

    public NewsAdapter(List<News> items, Context context) {
        dataset = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_news_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.news = dataset.get(position);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.news.isVisited){

                    Intent intent = new Intent(context, NewsActivity.class);
                    intent.putExtra("link", holder.news.link);
                    intent.putExtra("title", holder.news.title);
                    context.startActivity(intent);
                } else {

                    try {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ((AnimatedVectorDrawable) holder.mMarkReadView.getDrawable()).start();
                        } else {
                            ((AnimatedVectorDrawableCompat) holder.mMarkReadView.getDrawable()).start();
                        }
                    } catch (ClassCastException e){
                        e.printStackTrace();
                    }
                    holder.mMarkReadView.setVisibility(View.VISIBLE);
                    holder.mMarkReadView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(context, NewsActivity.class);
                            intent.putExtra("link", holder.news.link);
                            intent.putExtra("title", holder.news.title);
                            Utils.addVisitedNews(context, holder.news.link);
                            holder.news.isVisited = true;
                            notifyItemChanged(position);
                            context.startActivity(intent);
                        }
                    },  350 );
                }
            }
        });
        if (holder.news.isVisited){
            holder.mMarkReadView.setImageResource(R.drawable.mard_was_read);
        } else {
            holder.mMarkReadView.setImageResource(R.drawable.anim_mark_read);

        }

        SimpleDateFormat sdf = new SimpleDateFormat("EE, dd MMM yyyy hh:mm:ss Z", Locale.ENGLISH);
        try {
            Date date = sdf.parse(holder.news.date);
            sdf = new SimpleDateFormat("MMM dd. yyyy hh:mm", Locale.getDefault());
            String dateString = sdf.format(date);
            dateString = dateString.substring(0,1).toUpperCase() + dateString.substring(1).toLowerCase();

            holder.mDateView.setText(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mSourceView.setText(Html.fromHtml(holder.news.creator));
        String title = Html.fromHtml(holder.news.title).toString();

        if (title.indexOf('>') >=0){
            title = title.substring(title.indexOf('>'));
        }
        holder.mTitleView.setText(Html.fromHtml(title));

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setDataSet(ArrayList<News> dataSet) {
        this.dataset = dataSet;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mSourceView;
        public final TextView mDateView;
        public final TextView mTitleView;
        public final ImageView mMarkReadView;
        public News news;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDateView = (TextView) view.findViewById(R.id.tv_date);
            mSourceView = (TextView) view.findViewById(R.id.tv_source);
            mTitleView = (TextView) view.findViewById(R.id.tv_title);
            mMarkReadView = (ImageView) view.findViewById(R.id.mark_read);
        }

    }
}
