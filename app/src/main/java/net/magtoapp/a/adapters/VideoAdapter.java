package net.magtoapp.a.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedDrawable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jp.wasabeef.glide.transformations.BlurTransformation;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.activity.ChannelActivity;
import net.magtoapp.a.activity.PlayerActivity;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.VideoData;

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {


    public static final int REQUEST_VISITED_VIDEOS = 777;
    private static final int TYPE_DUMP_BOTTOM = 1;
    private static final int TYPE_VIDEO = 0;
    public static final String KEY_VISITED_NEWS = "keyVisitedNews";
    /*package*/ List<VideoData> dataset;
    private final Context context;
    private int mEndOffset;
    private int mStartOffset;

    public VideoAdapter(Context context, List<VideoData> objects) {
        super();
        setHasStableIds(true);
        this.context = context;
        this.dataset = objects;
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == getItemCount() - 1 - mEndOffset){
//            return TYPE_DUMP_BOTTOM;
//        }
        return TYPE_VIDEO;
    }

    public void setDataSetOffSet(int startOffset, int endOffset){
        mStartOffset = startOffset;
        mEndOffset = endOffset;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        view = inflater.inflate(R.layout.video_item, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
         if (viewHolder instanceof VideoViewHolder){

             final VideoData videoData = dataset.get(position - mStartOffset);
             final VideoViewHolder holder = (VideoViewHolder) viewHolder;

             holder.itemView.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View v) {
                     try {
                         if (context instanceof PlayerActivity){
                             ((PlayerActivity) context).onPreActivityLeft();
                         }
                         Intent intent = new Intent(context, PlayerActivity.class);
                         intent.putExtra("url", videoData.url);
//                         intent.putExtra("title", videoData.title);
//                         intent.putExtra("description", videoData.description);
                         intent.putExtra("channel", videoData.channel.title);
                         intent.putExtra("channelLogo", videoData.channel.humbnails._60x60);
//                         intent.putExtra("channelLogo", videoData.channel.humbnails._60x60);
//                         intent.putExtra("views", videoData.views);
                         intent.putExtra("channelId", videoData.channel.id);
//                         intent.putExtra("publishedAt", videoData.publishedAt);
//                         intent.putExtra("thumbnails", videoData.thumbnails._1280x720);
//                         intent.putExtra("like", videoData.likes);
//                         intent.putExtra("dislike", videoData.dislikes);
                         ((Activity)context).startActivityForResult(intent, REQUEST_VISITED_VIDEOS);
                     } catch (NullPointerException ignored) {
                     }
                 }
             });

             Glide.with(context)
                     .load(videoData.thumbnails._220x124)
                     .bitmapTransform(new BlurTransformation(context))
                     .into(holder.panelBlurBackground);


             Glide.with(context)
                     .load(videoData.thumbnails._568x320)
                     .into(holder.background);


             if (videoData.channel != null){
                 Glide.with(context)
                         .load(videoData.channel.humbnails._51x51)
                         .into(holder.channelLogo);
             }

             holder.channelLogo.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     Intent intent = new Intent(context, ChannelActivity.class);
                     intent.putExtra("id", videoData.channel.id);
                     intent.putExtra("title", videoData.channel.title);
                     intent.putExtra("description", "Description");
                     context.startActivity(intent);
                 }
             });

             if (holder.viewsTextView != null)
                 holder.viewsTextView.setText(videoData.views + "");

             holder.titleView.setText(videoData.title);
             SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
             try {
                 Date date = sdf.parse(videoData.publishedAt);
                 holder.dateView.setText(Utils.generateStringFromDate(date));
             } catch (ParseException e) {
                 e.printStackTrace();
             }


         } else {
             if (position == getItemCount() - 1 - mEndOffset){
                 //do nothing
             }
         }

    }

    @Override
    public int getItemCount() {
        return dataset.size()  + mStartOffset + mEndOffset;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setDataset(List<VideoData> dataset) {
        this.dataset = dataset;
        notifyDataSetChanged();
        notifyItemRangeChanged(0, dataset.size());
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (((VideoViewHolder) holder).background.getDrawable() instanceof RoundedDrawable)
            ((RoundedDrawable) ((VideoViewHolder) holder).background.getDrawable()).getSourceBitmap().recycle();
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        public TextView titleView;
        public TextView dateView;
//        public View viewsView;
        public View panelView;
        public TextView viewsTextView;
        public ImageView background;
        public ImageView channelLogo;
//        public ImageView backgroundShadow;
        public float expandedExtent = 2;
        public ImageView panelBlurBackground;

        public VideoViewHolder(View itemView) {
            super(itemView);
            dateView = (TextView) itemView.findViewById(R.id.video_item_date);
//            viewsView = itemView.findViewById(R.id.video_item_view);
            viewsTextView = (TextView) itemView.findViewById(R.id.panel_views);
            titleView = (TextView) itemView.findViewById(R.id.title);
            background = (ImageView) itemView.findViewById(R.id.background);
            channelLogo = (ImageView) itemView.findViewById(R.id.channel_logo);
//            backgroundShadow = (ImageView) itemView.findViewById(R.id.background_shadow);
            panelView = itemView.findViewById(R.id.panel);
            panelBlurBackground = (ImageView) itemView.findViewById(R.id.panel_blurred_background);
        }

        public void setExpandedExtent(float extent){

            if (getExpandedExtent() == extent)
                return;

            expandedExtent = extent;
            int specWidth = View.MeasureSpec.makeMeasureSpec(0 /* any */, View.MeasureSpec.UNSPECIFIED);
            itemView.measure(RecyclerView.LayoutParams.MATCH_PARENT, specWidth);
            channelLogo.setScaleX(1 + (extent / 4));
            channelLogo.setScaleY(1 + (extent / 4));
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            lp.height =  (int) ((0.5f + extent/2f) * itemView.getMeasuredHeight());
            itemView.setLayoutParams(lp);

            dateView.setAlpha(extent);
//            viewsView.setAlpha(extent);

//            extent = Math.max(1 - extent* 2, 0) ;

//            GradientDrawable gd = new GradientDrawable(
//                    GradientDrawable.Orientation.TOP_BOTTOM,
//                    new int[] {0, ((int)(0x99 * extent)) * 0x1000000,((int)(0x99 * extent)) * 0x1000000});
//            gd.setCornerRadius(0f);
//            panelView.setBackgroundDrawable(gd);


//            extent = 1 - extent;
//            backgroundShadow.setAlpha(extent);

        }


        float getExpandedExtent(){
            return expandedExtent;
        }
    }


}