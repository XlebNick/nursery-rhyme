package net.magtoapp.a.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.text.util.LinkifyCompat;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import net.magtoapp.a.activity.PlayerActivity;
import net.magtoapp.a.server.GeneralResponse;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.server.StupidXmlConverterFactory;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.VideoData;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends LoadableFragment implements Callback<VideoData>{

    private MyApiEndpointInterface apiService;
    private ImageView showDescription;

    private TextView titleView;
    private TextView descriptionView;
    private TextView channelView;
    private TextView dateView;
    private TextView likeCountView;
    private TextView dislikeCountView;
    private ImageView likeBtn;
    private ImageView dislikeBtn;
    private View likeView;
    private View dislikeView;
    private View content;
    private ImageView starBtn;
    private VideoData videoData;

    private boolean isStarred;

    public static final String EMPTY = "empty";
    public static final String LIKED = "like";
    public static final String DISLIKED = "dislike";
    private OnVideoDataLoadedListener listener;


    public DetailsFragment() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://media.magtoapp.net/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("dd-MM-yyyy hh:mm:ss").create()))
                .addConverterFactory(new StupidXmlConverterFactory())
                .build();

        apiService = retrofit.create(MyApiEndpointInterface.class);
    }

    public static DetailsFragment newInstance(String id, String channelId, String channelTitle, String humbnails){
        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("channelId", channelId);
        args.putString("channelTitle", channelTitle);
        args.putString("channelLogo60x60", humbnails);
        detailsFragment.setArguments(args);
        return detailsFragment;
    }

    private String getVideoId(){
        return getArguments().getString("id");
    }
    private String getChannelTitle(){
        return getArguments().getString("channelTitle");
    }
    private String getChannelId(){
        return getArguments().getString("channelId");
    }
    private String getChannelLogo60x60(){
        return getArguments().getString("channelLogo60x60");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_details, container, false);
        descriptionView = (TextView) view.findViewById(R.id.details_description);
        likeCountView = (TextView) view.findViewById(R.id.like_count);
        dislikeCountView = (TextView) view.findViewById(R.id.dislike_count);
        starBtn = (ImageView) view.findViewById(R.id.star);
        likeBtn = (ImageView) view.findViewById(R.id.like_btn);
        dislikeBtn = (ImageView) view.findViewById(R.id.dislike_btn);

        descriptionView.setMovementMethod(LinkMovementMethod.getInstance());

        starBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isStarred = Utils.isStarred(getContext(), videoData.id);
                if (isStarred){
                    Toast.makeText(getContext(), "Removed from bookmarks", Toast.LENGTH_SHORT).show();
                    Utils.removeStarredItem(getContext(), videoData);
                    starBtn.setImageResource(R.drawable.btn_star);

                } else {
                    Toast.makeText(getContext(), "Added to bookmarks", Toast.LENGTH_SHORT).show();
                    Utils.addStarredItem(getContext(), videoData);
                    starBtn.setImageResource(R.drawable.btn_starred);
                }
            }
        });

        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performLikeOrDislike(true);
            }
        });
        dislikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performLikeOrDislike(false);
            }
        });

        return super.onCreateView(inflater, container, savedInstanceState, view);
    }

    @Override
    public void onResponse(Call<VideoData> call, Response<VideoData> response) {
        if (response != null && response.body() != null && response.code() == 200){
            this.videoData = response.body();
//            titleView.setText(videoData.title);
//            channelView.setText(getChannelTitle());
//            dateView.setText(new SimpleDateFormat("hh:mm, dd.MM.yyyy").format(videoData.publishedAt));

            //substract user's vote from votes
            if (videoData.voted == null){
                videoData.voted = EMPTY;
            }
            if (videoData.voted.equals(LIKED)){
                videoData.likes--;
            }
            if (videoData.voted.equals(DISLIKED)){
                videoData.dislikes--;
            }
            recalculateCounters();

            Log.d("***", videoData.voted + " voted");
            if (TextUtils.isEmpty(videoData.description)){
                descriptionView.setText("No description");
            } else {
                descriptionView.setText(videoData.description);
            }

            LinkifyCompat.addLinks(descriptionView, Linkify.WEB_URLS);
//            stripUnderlines(descriptionView);

            videoData.timeOfWatching = System.currentTimeMillis();
            videoData.channel.title = getChannelTitle();
            videoData.channel.id = getChannelId();

            Utils.addHistoryItem(getContext(), videoData);

            isStarred = Utils.isStarred(getContext(), videoData.id);
            if (isStarred){
                starBtn.setImageResource(R.drawable.btn_starred);
            } else {
                starBtn.setImageResource(R.drawable.btn_star);
            }
            if (listener == null && getContext() instanceof PlayerActivity) {
                listener = ((PlayerActivity) getContext());
            }
            if (listener != null)
            listener.onVideoDataLoaded(videoData);
            onLoaded();
        } else {
            Toast.makeText(getContext(), "Error during retrieving description", Toast.LENGTH_LONG).show();
        }
    }

    private void recalculateCounters(){

        likeCountView.setText(String.valueOf(getLikeCount()));
        dislikeCountView.setText(String.valueOf(getDislikeCount()));
        if (videoData.voted.equals(LIKED)){
            likeBtn.setImageResource(R.drawable.btn_liked);
            dislikeBtn.setImageResource(R.drawable.btn_dislike);
        }
        if (videoData.voted.equals(DISLIKED)){
            likeBtn.setImageResource(R.drawable.btn_like);
            dislikeBtn.setImageResource(R.drawable.btn_disliked);
        }
        if (videoData.voted.equals(EMPTY)){

            likeBtn.setImageResource(R.drawable.btn_like);
            dislikeBtn.setImageResource(R.drawable.btn_dislike);
        }
        if (listener != null)
            listener.onVideoDataLoaded(videoData);
    }

    private int getLikeCount(){
        return videoData.voted.equals(LIKED) ? videoData.likes + 1 : videoData.likes;
    }
    private int getDislikeCount(){
        return videoData.voted.equals(DISLIKED) ? videoData.dislikes + 1 : videoData.dislikes;
    }

    @Override
    public void onFailure(Call<VideoData> call, Throwable t) {
        t.printStackTrace();
        onFailed();
    }

    @Override
    protected void onLoadStart() {
        currentCall = apiService.getVideoInfo(getVideoId(), Utils.getDeviceId(getContext()), 1,
                Utils.getAppId(getContext()));
        currentCall.enqueue(this);
    }


    private void performLikeOrDislike(final boolean isLike){

        if (videoData.voted.equals(isLike ? LIKED : DISLIKED)){
            videoData.voted = EMPTY;
            recalculateCounters();
            apiService.unvote(videoData.id, Utils.getDeviceId(getContext()),
                    Utils.getToken(getContext()),
                    Utils.getAppId(getContext())).enqueue(new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    if (!response.isSuccessful()){
                        videoData.voted = isLike ? LIKED : DISLIKED;
                        recalculateCounters();
                        Toast.makeText(getContext(), "Произошла ошибка при снятии " + (isLike ? "" : "диз") + "лайка", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    t.printStackTrace();
                    videoData.voted = LIKED;
                    recalculateCounters();
                    Toast.makeText(getContext(), "Произошла ошибка при снятии " + (isLike ? "" : "диз") + "лайка", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            final String previousState = videoData.voted;
            videoData.voted = isLike ? LIKED : DISLIKED;
            recalculateCounters();
            Callback<GeneralResponse> callback = new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    if (!response.isSuccessful()){
                        videoData.voted = previousState;
                        recalculateCounters();
                        Toast.makeText(getContext(), "Произошла ошибка при " + (isLike ? "" : "диз") + "лайке", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    t.printStackTrace();
                    videoData.voted = previousState;
                    recalculateCounters();
                    Toast.makeText(getContext(), "Произошла ошибка при " + (isLike ? "" : "диз") + "лайке", Toast.LENGTH_SHORT).show();
                }
            };
            if (isLike){
                apiService.like(videoData.id, Utils.getDeviceId(getContext()),
                        Utils.getToken(getContext())).enqueue(callback);
            } else {
                apiService.dislike(videoData.id, Utils.getDeviceId(getContext()),
                        Utils.getToken(getContext()),
                        Utils.getAppId(getContext())).enqueue(callback);
            }
        }
    }
//    private void stripUnderlines(TextView textView) {
//        Spannable s = new SpannableString(textView.getText());
//        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//        for (URLSpan span: spans) {
//            int start = s.getSpanStart(span);
//            int end = s.getSpanEnd(span);
//            s.removeSpan(span);
//            span = new URLSpanNoUnderline(span.getURL());
//            s.setSpan(span, start, end, 0);
//        }
//        textView.setText(s);
//    }

    public void setOnVideoDataLoadedListener(OnVideoDataLoadedListener listener) {
        this.listener = listener;
    }

    public interface OnVideoDataLoadedListener{
        void onVideoDataLoaded(VideoData videoData);
    }

//    @SuppressLint("ParcelCreator")
//    private class URLSpanNoUnderline extends URLSpan {
//        public URLSpanNoUnderline(String url) {
//            super(url);
//        }
//        @Override public void updateDrawState(TextPaint ds) {
//            super.updateDrawState(ds);
//            ds.setUnderlineText(false);
//        }
//    }
}
