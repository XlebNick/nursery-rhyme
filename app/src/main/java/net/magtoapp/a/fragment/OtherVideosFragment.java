package net.magtoapp.a.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.activity.PlayerActivity;
import net.magtoapp.a.adapters.VideoAdapter;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.recyclervieweffects.LargeTopItemOnScrollListener;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class OtherVideosFragment extends LoadableFragment implements Callback<ArrayList<VideoData>>,PlayerActivity.OnVideoPlayedListener {

    private Call<ArrayList<VideoData>> currentCall;
    private MyApiEndpointInterface apiService;
    public RecyclerView recyclerView;
    public ArrayList<VideoData> dataset;
    public ArrayList<String> visitedVideos;


    public OtherVideosFragment() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://media.magtoapp.net/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("dd-MM-yyyy hh:mm:ss").create()))
                .build();

        apiService = retrofit.create(MyApiEndpointInterface.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
//            recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
//            SnapHelper snapHelperTop = new LinearSnapHelper();
//            snapHelperTop.attachToRecyclerView(recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            visitedVideos = new ArrayList<>();
            visitedVideos.add(getVideoId());

            recyclerView.addOnScrollListener(new LargeTopItemOnScrollListener());
            recyclerView.setAdapter(new VideoAdapter(getActivity(), new ArrayList<VideoData>() ){
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                    LayoutInflater inflater = (LayoutInflater) getContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view;
                    view = inflater.inflate(R.layout.other_video_item, parent, false);
                    return new VideoViewHolder(view);

                }
            });
        }

        return super.onCreateView(inflater, container, savedInstanceState, view);
    }

    @Override
    public void onLoadStart() {
        String channelId = getChannelId();
        if (TextUtils.isEmpty(channelId)){

            currentCall = apiService.getAllVideos(Utils.getAppId(getContext()));
        } else {

            currentCall = apiService.getVideosFromCategory(Utils.getDeviceId(getContext()),
                    Utils.getAppId(getContext()),
                    channelId);
        }
        Log.d("***", "errer " + currentCall.request().url() + " " + currentCall.request().body());
        currentCall.enqueue(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        new AsyncTask<Call, Void, Void>() {
            @Override
            protected Void doInBackground(Call... params) {
                Call call = params[0];
                call.cancel();
                return null;
            }
        }.execute(currentCall);
    }

    @Override
    public void onDestroy() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(VideoAdapter.KEY_VISITED_NEWS, visitedVideos);
        getActivity().setResult(RESULT_OK, resultIntent);
        if (getActivity().getParent() == null) {
            getActivity().setResult(RESULT_OK, resultIntent);
        } else {
            getActivity().getParent().setResult(RESULT_OK, resultIntent);
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VideoAdapter.REQUEST_VISITED_VIDEOS){
            if (resultCode == RESULT_OK){
                ArrayList<String> visitedVideos = data.getStringArrayListExtra(VideoAdapter.KEY_VISITED_NEWS);
                for (String newsId : visitedVideos){
                    for (VideoData videoData : dataset){
                        if (videoData.id.equals(newsId)){
                            videoData.views++;
                        }
                    }
                    Log.d("***", visitedVideos.size() + " " + newsId);
                }
                visitedVideos.add(getVideoId());
                this.visitedVideos = visitedVideos;
                ((VideoAdapter) recyclerView.getAdapter()).setDataset(dataset);
            }
        }
    }

    public static OtherVideosFragment newInstance(String id, String channelId, String channelTitle) {

        Bundle args = new Bundle();
        args.putString("channelId", channelId);
        args.putString("channelTitle", channelTitle);
        args.putString("id", id);
        OtherVideosFragment fragment = new OtherVideosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String getChannelId(){
        return getArguments().getString("channelId", "");
    }


    private String getVideoId(){
        return getArguments().getString("id", "");
    }


    public String getChannelTitle() {
        return getArguments().getString("channelTitle", "");
    }

    @Override
    public void onResponse(Call<ArrayList<VideoData>> call, Response<ArrayList<VideoData>> response) {
        if (response == null || response.body() == null || response.code() != 200) {

            Log.d("***", "errer + " + new Gson().toJson(response));
            onFailed();
        } else {
            ArrayList<VideoData> dataset = response.body();
            for (VideoData videoData : dataset){
                if (videoData.id.equals(getVideoId())){
                    dataset.remove(videoData);
                    break;
                }
            }
            for (VideoData videoData : dataset){
                videoData.channel.id = getChannelId();
                videoData.channel.title = getChannelTitle();
            }
            ((VideoAdapter) recyclerView.getAdapter()).setDataset(dataset);
            recyclerView.getAdapter().notifyDataSetChanged();
            this.dataset = dataset;
            onLoaded();
        }
    }

    @Override
    public void onFailure(Call<ArrayList<VideoData>> call, Throwable t) {
        t.printStackTrace();
        onFailed();
    }

    @Override
    public void onPlayed(boolean isPlayed) {
//        if (isPlayed){
//            recyclerView.setVisibility(View.GONE);
//        } else {
//            recyclerView.setVisibility(View.VISIBLE);
//        }
    }
}
