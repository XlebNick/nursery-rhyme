package net.magtoapp.a.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.adapters.CommentsAdapter;
import net.magtoapp.a.server.MyApiEndpointInterface;
import net.magtoapp.a.server.StupidXmlConverterFactory;
import net.magtoapp.a.controller.Utils;
import net.magtoapp.a.model.Comment;
import net.magtoapp.a.activity.PlayerActivity;

public class CommentsFragment extends LoadableFragment implements Callback<ArrayList<Comment>>,PlayerActivity.OnCommentAddedListener {
    private static final String ARG_PARAM1 = "id";


    public static final String EMPTY = "empty";
    public static final String LIKED = "liked";
    public static final String DISLIKED = "disliked";
    private MyApiEndpointInterface apiService;

    private RecyclerView recyclerView;

    public CommentsFragment() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://media.magtoapp.net/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("dd-MM-yyyy hh:mm:ss ZZZZ").create()))
                .addConverterFactory(new StupidXmlConverterFactory())
                .build();

        apiService = retrofit.create(MyApiEndpointInterface.class);
    }

    // TODO: Rename and change types and number of parameters
    public static CommentsFragment newInstance(String videoId) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, videoId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new CommentsAdapter(getContext(), new ArrayList<Comment>()));

        view.findViewById(R.id.add_comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((PlayerActivity) getActivity()).showAddCommentView();
            }
        });
        return super.onCreateView(inflater, container, savedInstanceState, view);
    }

    private String getVideoId(){
        return getArguments().getString(ARG_PARAM1);
    }

    @Override
    public void onResponse(Call<ArrayList<Comment>> call, Response<ArrayList<Comment>> response) {
        if (response != null && response.body() != null && response.code() == 200) {

            ArrayList<Comment> comments = response.body();
            ArrayList<Comment> reversecomments = new ArrayList<>();
            for (int i = 0; i < comments.size(); i++){

                Comment comment = comments.get(i);
                if (comment.voted == null){
                    comment.voted = EMPTY;
                }
                if (comment.voted.equals(LIKED)){
                    comment.likes--;

                }
                if (comment.voted.equals(DISLIKED)){
                    comment.dislikes--;
                }
                reversecomments.add(0, comment);
            }

            ((CommentsAdapter) recyclerView.getAdapter()).setDataset(reversecomments);
            recyclerView.getAdapter().notifyDataSetChanged();
            onLoaded();
        } else {
            onFailed();
        }

    }

    @Override
    public void onFailure(Call<ArrayList<Comment>> call, Throwable t) {
        t.printStackTrace();
        onFailed();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onLoadStart() {
        currentCall = apiService.getComments(getVideoId(), 1, 100,
                Utils.getEmail(getContext()),
                Utils.getToken(getContext()),
                Utils.getDeviceId(getContext()),
                Utils.getAppId(getContext()));
        currentCall.enqueue(this);
    }

    @Override
    public void onCommentAdded() {
        onLoadStart();
    }
}
