package net.magtoapp.a.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.magtoapp.xlebnick.a.R;
import net.magtoapp.a.model.HistoryItem;
import net.magtoapp.a.model.VideoData;
import net.magtoapp.a.server.MyApiEndpointInterface;
import trikita.log.Log;

/**
 * Created by XlebNick for Carousel.
 */
public class Utils {

    public static final String SHARED_PREFERENCES_NAME = "magtoapp_carousel";
    public static final String SHARED_PREFERENCES_VISITED_NEWS = "visited_news";
    private static final String SHARED_PREFERENCES_HISTORY_ITEMS = "history_items";
    private static final String SHARED_PREFERENCES_VIEWPAGER_DATASET = "viewpager_dataset";
    private static final String SHARED_PREFERENCES_STARRED_ITEMS = "starred_items";
    private static final String SHARED_PREFERENCES_LIKED_VIDEOS = "liked_videos";
    private static final String SHARED_PREFERENCES_DISLIKED_VIDEOS = "disliked_videos";
    private static final String SHARED_PREFERENCES_TOKEN = "token";
    private static final String SHARED_PREFERENCES_EMAIL = "email";
    private static final String SHARED_PREFERENCES_LAST_TIME_MAIN = "last_time_main";
    private static final String SHARED_PREFERENCES_USERNAME = "username";
    private static final String SHARED_PREFERENCES_LAST_TIME_CATEGRIES_LOADED = "last_time_categories_loaded";
    private static final String SHARED_PREFERENCES_CATEGORIES_COUNTER_SET = "categories_counter_set";
    private static final String SHARED_PREFERENCES_LAST_TIME_AD_SHOWN = "last_time_ad_shown";
    private static Gson gson = new Gson();

    public static SharedPreferences getSPrefs(Context context){
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static Set<String> getVisitedNews(Context context){

        return getSPrefs(context).getStringSet(SHARED_PREFERENCES_VISITED_NEWS, new HashSet<String>());
    }

    public static ArrayList<VideoData> getHistoryItems(Context context){
        Object[] strings =  getSPrefs(context).getStringSet(SHARED_PREFERENCES_HISTORY_ITEMS, new HashSet<String>()).toArray();
        ArrayList<VideoData> historyItems = new ArrayList<>();
        for (Object string : strings){
            try{
                VideoData item = gson.fromJson(string.toString(), VideoData.class);
                historyItems.add(item);
            } catch (com.google.gson.JsonSyntaxException e){
                e.printStackTrace();
            }
        }
        return historyItems;
    }
    public static ArrayList<VideoData> getStarredItems(Context context){
        Object[] strings =  getSPrefs(context).getStringSet(SHARED_PREFERENCES_STARRED_ITEMS, new HashSet<String>()).toArray();
        ArrayList<VideoData> historyItems = new ArrayList<>();
        for (Object string : strings){
            try{
                VideoData item = gson.fromJson(string.toString(), VideoData.class);
                historyItems.add(item);
            } catch (com.google.gson.JsonSyntaxException e){
                e.printStackTrace();
            }
        }
        return historyItems;
    }


    public static boolean addStarredItem(Context context, VideoData object){

        int deleted = 0;
        try {
            Set<String> historyItems = getSPrefs(context).getStringSet(SHARED_PREFERENCES_STARRED_ITEMS, new HashSet<String>());

            VideoData item;
            for (String string : historyItems){
                try{
                    item = gson.fromJson(string, VideoData.class);
                    if(item.url.equals(object.url)){
                        historyItems.remove(string);
                        deleted++;
                        break;
                    }
                } catch (JsonSyntaxException e){
                    e.printStackTrace();
                    getSPrefs(context).edit().remove(SHARED_PREFERENCES_STARRED_ITEMS).apply();
                    historyItems = new HashSet<>();
                    historyItems.add(gson.toJson(object));
                    getSPrefs(context).edit().putStringSet(SHARED_PREFERENCES_STARRED_ITEMS, historyItems).apply();
                    return true;
                }
            }
            if (deleted == 0){
                historyItems.add(gson.toJson(object));
            }

            getSPrefs(context).edit()
                    .remove(SHARED_PREFERENCES_STARRED_ITEMS)
                    .apply();
            getSPrefs(context).edit()
                    .putStringSet(SHARED_PREFERENCES_STARRED_ITEMS, historyItems)
                    .apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deleted ==0;
    }

    public static void saveUserName(Context context, String userName){
        getSPrefs(context).edit().putString(SHARED_PREFERENCES_USERNAME, userName).commit();
    }

    public static String getUserName(Context context){
        return getSPrefs(context).getString(SHARED_PREFERENCES_USERNAME, "");
    }

    public static void addVisitedNews(Context context, String link){

        Set<String> visitedNews = getVisitedNews(context);
        visitedNews.add(link);
        getSPrefs(context).edit()
                .remove(SHARED_PREFERENCES_VISITED_NEWS)
                .apply();
        getSPrefs(context).edit()
                .putStringSet(SHARED_PREFERENCES_VISITED_NEWS, visitedNews)
                .apply();

    }

    public static boolean addHistoryItem(Context context, VideoData object){

        int deleted = 0;
        try {
            Set<String> historyItems = getSPrefs(context).getStringSet(SHARED_PREFERENCES_HISTORY_ITEMS, new HashSet<String>());

            VideoData item;
            for (String string : historyItems){
                try{
                    item = gson.fromJson(string, VideoData.class);
                    if(item.url.equals(object.url)){
                        historyItems.remove(string);
                        deleted++;
                        break;
                    }
                } catch (com.google.gson.JsonSyntaxException e){
                    e.printStackTrace();
                    getSPrefs(context).edit().remove(SHARED_PREFERENCES_HISTORY_ITEMS).apply();
                    historyItems = new HashSet<>();
                    historyItems.add(gson.toJson(object));
                    getSPrefs(context).edit().putStringSet(SHARED_PREFERENCES_HISTORY_ITEMS, historyItems).apply();
                    return true;
                }
            }
              if (deleted ==0)
                  object.views++;

            historyItems.add(gson.toJson(object));
            getSPrefs(context).edit()
                    .remove(SHARED_PREFERENCES_HISTORY_ITEMS)
                    .apply();
            getSPrefs(context).edit()
                    .putStringSet(SHARED_PREFERENCES_HISTORY_ITEMS, historyItems)
                    .apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deleted ==0;
    }

    public static void saveViewPagerDataset(Context context, ArrayList<VideoData> dataset){
        getSPrefs(context).edit()
                .putString(SHARED_PREFERENCES_VIEWPAGER_DATASET, new Gson().toJson(dataset))
                .commit();
    }

    public static ArrayList<VideoData> getSavedViewPagerDataset(Context context){
        ArrayList<VideoData> dataset = new ArrayList<>();
        VideoData[] videoDatas = new Gson().fromJson(getSPrefs(context).getString(SHARED_PREFERENCES_VIEWPAGER_DATASET, ""), VideoData[].class);
        if (videoDatas == null){
            videoDatas = new VideoData[]{};
        }
        dataset.addAll(Arrays.asList(videoDatas));
        return dataset;
    }

    public static boolean wasShownLastHour(Context context, String url){
        Set<String> historyItems = getSPrefs(context).getStringSet(SHARED_PREFERENCES_HISTORY_ITEMS, new HashSet<String>());

        HistoryItem item;
        for (String string : historyItems){
            item = gson.fromJson(string, HistoryItem.class);
            if (item.url.equals(url) && System.currentTimeMillis() - item.timeofWatching < 3600000){
                return true;
            }
        }
        return false;
    }

    public static String formatStringFromDate(String timeFromServer){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        try {
            Date date = sdf.parse(timeFromServer);
            long diff =  System.currentTimeMillis() - date.getTime();

            String textToInsert;

            if (diff < 86400000){
                textToInsert = diff / 3600000 + "ч. назад";
            } else if (diff < 172800000){
                textToInsert = "1 день назад";
            } else if (diff < 345600000){
                textToInsert = diff / 86400000 + " дня назад";
            } else if (Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("english")){
                sdf = new SimpleDateFormat("MMM dd yyyy HH:mm", Locale.getDefault());
                textToInsert = sdf.format(date);
            } else {
                sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
                textToInsert = sdf.format(date);
            }
            return textToInsert;
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isTablet(Context context){
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        boolean isTablet = diagonalInches >= 6.5;
        return isTablet;
//        return (context.getResources().getConfiguration().screenLayout
//                & Configuration.SCREENLAYOUT_SIZE_MASK)
//                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
//        return context.getResources().getBoolean(R.bool.isTab);
    }

    public static boolean isAspect16to9(Activity activity){

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float largeSide = Math.max(metrics.heightPixels, metrics.widthPixels);
        float smallSide = Math.min(metrics.heightPixels, metrics.widthPixels);

        return largeSide / smallSide > 1.5f;

    }

    public static String getDash(final String url){

        try {
            return new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
//                    OkHttpClient client = new OkHttpClient();
//                    Request request = new Request.Builder()
//                            .url("http://www.youtube.com/get_video_info?&video_id=" + url)
//                            .build();
//                    Response response ;
//                    String dash = "";
//                    try {
//                        response = client.newCall(request).execute();
//                        dash = response.body().string();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        return "";
//                    }
//
//                    try {
//                        dash = URLDecoder.decode(URLDecoder.decode(URLDecoder.decode(dash, "UTF-8"), "UTF-8"), "UTF-8");
//                        Log.d("dash is here1", dash);
//                        dash = dash.substring(dash.indexOf("dashmpd"));
//                        dash = dash.substring(8, dash.indexOf("&"));
//                    } catch (UnsupportedEncodingException|StringIndexOutOfBoundsException e){
//                        e.printStackTrace();
//                        return "";
//                    }
//                    return dash;


                    String dash = "";
                    new Retrofit.Builder()
                            .baseUrl("http://www.youtube.com")
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .build().create(MyApiEndpointInterface.class).getYoutubeManifest(url).enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                            if (response.isSuccessful() && response.body() != null){
                                try {
                                    String dash = URLDecoder.decode(URLDecoder.decode(URLDecoder.decode(response.body(), "UTF-8"), "UTF-8"), "UTF-8");
                                    Log.d("dash is here1", dash);
                                    dash = dash.substring(dash.indexOf("dashmpd"));
                                    dash = dash.substring(8, dash.indexOf("&"));
                                    Log.d("dash is here2", dash);
                                } catch (UnsupportedEncodingException|StringIndexOutOfBoundsException e){
                                    e.printStackTrace();
                                }
                            }
                            Log.d("***", "dash is here no no");
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.d("***", "dash is here no");
                            t.printStackTrace();
                        }
                    });

                    return dash;
                }
            }.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return "";
        }
    }


    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static int wasRated(Context context, String url) {
        Set<String> likedVideosUrls = getSPrefs(context).getStringSet(SHARED_PREFERENCES_LIKED_VIDEOS, new HashSet<String>());
        Set<String> dislikedVideosUrls = getSPrefs(context).getStringSet(SHARED_PREFERENCES_DISLIKED_VIDEOS, new HashSet<String>());
        int like = 0;
        if (likedVideosUrls.contains(url))
            like = 1;
        if (dislikedVideosUrls.contains(url))
            like = -1;
        return like;
    }
    public static void addRated(Context context, String url, boolean like) {
        String key = like ? SHARED_PREFERENCES_LIKED_VIDEOS : SHARED_PREFERENCES_DISLIKED_VIDEOS;
        String keyToDelete= like ? SHARED_PREFERENCES_DISLIKED_VIDEOS : SHARED_PREFERENCES_LIKED_VIDEOS;
        Set<String> ratedVideosUrls = getSPrefs(context).getStringSet(key, new HashSet<String>());

        ratedVideosUrls.add(url);
        getSPrefs(context).edit()
                .remove(key)
                .apply();
        getSPrefs(context).edit()
                .putStringSet(key, ratedVideosUrls)
                .apply();

        ratedVideosUrls = getSPrefs(context).getStringSet(keyToDelete, new HashSet<String>());
        ratedVideosUrls.remove(url);

        getSPrefs(context).edit()
                .remove(keyToDelete)
                .apply();
        getSPrefs(context).edit()
                .putStringSet(keyToDelete, ratedVideosUrls)
                .apply();
    }
    public static void putToken(Context context, String token) {
        SharedPreferences sPrefs = getSPrefs(context);
        if (sPrefs.contains(SHARED_PREFERENCES_TOKEN)){
            sPrefs.edit().remove(SHARED_PREFERENCES_TOKEN).apply();
        }
        sPrefs.edit().putString(SHARED_PREFERENCES_TOKEN, token).apply();
    }
    public static void putEmail(Context context, String email) {
        SharedPreferences sPrefs = getSPrefs(context);
        if (!sPrefs.contains(SHARED_PREFERENCES_EMAIL)){
            sPrefs.edit().remove(SHARED_PREFERENCES_EMAIL).apply();
        }

        sPrefs.edit().putString(SHARED_PREFERENCES_EMAIL, email).apply();
    }


    public static String getEmail(Context context) {
        return getSPrefs(context).getString(SHARED_PREFERENCES_EMAIL, null);
    }


    public static String getToken(Context context) {
        return getSPrefs(context).getString(SHARED_PREFERENCES_TOKEN, null);
    }

    public static boolean isTokenExists(Context context){
        return getSPrefs(context).contains(SHARED_PREFERENCES_TOKEN);
    }

    public static void setLastTimeMainPageRefreshed(Context context, long l) {
        getSPrefs(context).edit().putLong(SHARED_PREFERENCES_LAST_TIME_MAIN, l).apply();
    }
    public static long getLastTimeMainPageRefreshed(Context context) {
        return getSPrefs(context).getLong(SHARED_PREFERENCES_LAST_TIME_MAIN, 0);
    }

    public static String generateStringFromDate(Date date){

        date = new Date(date.getTime());
        SimpleDateFormat sdf;
        String textToInsert;
        long diff =  System.currentTimeMillis() - date.getTime();
        if (diff < 300000){
            textToInsert = "Now" ;
        } else if (diff < 3600000){
            textToInsert = diff / 60000 + " min. ago";
        } else if (diff < 86400000){
            textToInsert = diff / 3600000 + " h. ago";
        } else if (diff < 604800000){
            textToInsert =  Math.round(diff / 86400000f )+ " day" + (Math.round(diff / 86400000f ) == 1 ? "" : "s") + " ago";
        } else if (diff < 2678400000L) {
            textToInsert = Math.round(diff / 604800000f ) + " week" + (Math.round(diff / 604800000f ) == 1 ? "" : "s") + " ago";
        } else {
            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(date);
            Calendar endCalendar = new GregorianCalendar();
            endCalendar.setTimeInMillis(System.currentTimeMillis());

            int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
            int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

            textToInsert = diffMonth + " month" + (diffMonth == 1 ? "" : "s") + " ago";
        }
//        } else if (Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("english")){
//            sdf = new SimpleDateFormat("MMM dd yyyy HH:mm", Locale.getDefault());
//            textToInsert = sdf.format(date);
//        } else {
//            sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
//            textToInsert = sdf.format(date);
//        }

        return textToInsert;
    }

    public static void convertAllHistoryDataToVideoDataInSaved(Context context, String category){
        String[] historyItems = getSPrefs(context).getStringSet(category, new HashSet<String>()).toArray(new String[]{});
        for (int i = 0; i < historyItems.length; i ++){
            try {
                gson.fromJson(historyItems[i], VideoData.class);
            } catch (JsonSyntaxException e){
                HistoryItem historyItem = gson.fromJson(historyItems[i], HistoryItem.class);
                VideoData videoData = new VideoData();
                videoData.timeOfWatching = historyItem.timeofWatching;
                videoData.channel.id = historyItem.channelId;
                videoData.title = historyItem.title;
                videoData.description = historyItem.description;
                videoData.channel.title = historyItem.channel;
                videoData.views = historyItem.views;
                videoData.thumbnails._1280x720 = historyItem.thumbnails;
                videoData.publishedAt = historyItem.publishedAt;
                videoData.url = historyItem.url;
                videoData.url = historyItem.url;
                videoData.url = historyItem.url;
                videoData.url = historyItem.url;
                videoData.likes = historyItem.like;
                videoData.dislikes = historyItem.dislike;
                historyItems[i] = gson.toJson(videoData);
            }
        }
        Set<String> newStringSet = new HashSet<>(Arrays.asList(historyItems));
        getSPrefs(context).edit().remove(category).apply();
        getSPrefs(context).edit().putStringSet(category, newStringSet).apply();

    }

    public static void convertAll(Context context){
        convertAllHistoryDataToVideoDataInSaved(context, SHARED_PREFERENCES_HISTORY_ITEMS);
        convertAllHistoryDataToVideoDataInSaved(context, SHARED_PREFERENCES_STARRED_ITEMS);
    }

    public static boolean isVideoStarred(Context context, String id) {
        ArrayList<VideoData> dataset = getStarredItems(context);
        for (VideoData videoData : dataset){
            if (id.equals(videoData.id))
                return true;
        }
        return false;
    }

    public static void removeStarredItem(Context context, VideoData object) {
        try {
            Set<String> historyItems = getSPrefs(context).getStringSet(SHARED_PREFERENCES_STARRED_ITEMS, new HashSet<String>());

            VideoData item;
            for (String string : historyItems){
                try{
                    item = gson.fromJson(string, VideoData.class);
                    if(item.url.equals(object.url)){
                        historyItems.remove(string);
                        break;
                    }
                } catch (JsonSyntaxException e){
                    e.printStackTrace();
                    getSPrefs(context).edit().remove(SHARED_PREFERENCES_STARRED_ITEMS).apply();
                    historyItems = new HashSet<>();
                    historyItems.add(gson.toJson(object));
                    getSPrefs(context).edit().putStringSet(SHARED_PREFERENCES_STARRED_ITEMS, historyItems).apply();
                }
            }

            getSPrefs(context).edit()
                    .remove(SHARED_PREFERENCES_STARRED_ITEMS)
                    .apply();
            getSPrefs(context).edit()
                    .putStringSet(SHARED_PREFERENCES_STARRED_ITEMS, historyItems)
                    .apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getAppId(Context context) {
//        return "net.magtoapp.carousel";
        return context.getString(R.string.application_id);
    }

    public static long getLastTimeCategoriesLoaded(Context context) {
        return getSPrefs(context).getLong(SHARED_PREFERENCES_LAST_TIME_CATEGRIES_LOADED, 0);
    }
    public static void putLastTimeCategoriesLoaded(Context context, long lastTimeSeconds) {
        getSPrefs(context).edit().putLong(SHARED_PREFERENCES_LAST_TIME_CATEGRIES_LOADED, lastTimeSeconds).commit();
    }

    public static HashMap<String, Integer> getCategoriesCounterMap(Context context){
        String string = getSPrefs(context).getString(SHARED_PREFERENCES_CATEGORIES_COUNTER_SET, "");
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] entries = string.split(",");
        if (string.length() < 2) return hashMap;
        for (String entry : entries){
            String[] item = entry.split("=");
            hashMap.put(item[0].trim(), Integer.valueOf(item[1].trim()));
        }
        return hashMap;
    }
    public static void puttCategoriesCounterMap(Context context, HashMap<String, Integer> hashMap){
        String string = hashMap.toString();
        string = string.substring(1, string.lastIndexOf("}"));
        getSPrefs(context).edit().putString(SHARED_PREFERENCES_CATEGORIES_COUNTER_SET, string).commit();
    }

    public static boolean isStarred(Context context, String videoDataId){
        Object[] strings =  getSPrefs(context).getStringSet(SHARED_PREFERENCES_STARRED_ITEMS, new HashSet<String>()).toArray();
        for (Object string : strings){
            VideoData item = gson.fromJson(string.toString(), VideoData.class);
            if (item.id.equals(videoDataId)){
                return true;
            }
        }
        return false;
    }

    public static long getLastTimeAdShown(Context context) {
        return getSPrefs(context).getLong(SHARED_PREFERENCES_LAST_TIME_AD_SHOWN, 0);
    }

    public static void setLastTimeAdShown(Context context, long timeMillis) {
        getSPrefs(context).edit().putLong(SHARED_PREFERENCES_LAST_TIME_AD_SHOWN, timeMillis).apply();
    }

    public static String getDashFromManifest(String body) {
        String dash = "";
        try {
            dash = URLDecoder.decode(URLDecoder.decode(URLDecoder.decode(body, "UTF-8"), "UTF-8"), "UTF-8");
            Log.d("dash is here1", dash);
            dash = dash.substring(dash.indexOf("dashmpd"));
            dash = dash.substring(8, dash.indexOf("&"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return dash;
    }
}
