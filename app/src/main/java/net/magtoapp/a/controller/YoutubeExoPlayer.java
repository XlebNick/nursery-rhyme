package net.magtoapp.a.controller;

import android.os.Handler;
import android.view.Surface;

import com.google.android.exoplayer.DummyTrackRenderer;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.util.PlayerControl;

/**
 * Created by XlebNick for Carousel.
 */
public class YoutubeExoPlayer {

    public static final int RENDERER_COUNT = 2;
    public static final int TYPE_VIDEO = 0;
    public static final int TYPE_AUDIO = 1;

    private final ExoPlayer player;
    private final DashRendererBuilder rendererBuilder;
    private final Handler mainHandler;
    private final PlayerControl playerControl;

    private Surface surface;
    private TrackRenderer videoRenderer;

    public YoutubeExoPlayer(DashRendererBuilder rendererBuilder){
        player = ExoPlayer.Factory.newInstance(RENDERER_COUNT, 1000, 5000);
        playerControl = new PlayerControl(player);
        this.rendererBuilder = rendererBuilder;
        mainHandler = new Handler();

    }

    public void setSurface(Surface surface){
        this.surface = surface;
    }

    private void pushSurface(boolean blockForSurfacePush){
        if (videoRenderer == null) {
            return;
        }

        if (blockForSurfacePush) {
            player.blockingSendMessage(
                    videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, surface);
        } else {
            player.sendMessage(
                    videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, surface);
        }
    }

    public void release() {
        rendererBuilder.cancel();
        surface = null;
        player.release();
    }

    /* package */ void onRenderers(TrackRenderer[] renderers){
        for (int i = 0; i < RENDERER_COUNT; i++){
            if (renderers[i] == null){
                renderers[i] = new DummyTrackRenderer();
            }
        }

        this.videoRenderer = renderers[TYPE_VIDEO];
        pushSurface(false);
        player.prepare(renderers);

    }

    public void setPlayWhenReady(boolean playWhenReady){
        player.setPlayWhenReady(playWhenReady);
    }

    public void seekTo(long positionMs) {
        player.seekTo(positionMs);
    }

    public Handler getMainHandler() {
        return mainHandler;
    }

    public void prepare() {
        rendererBuilder.cancel();
        videoRenderer = null;
        rendererBuilder.buildRenderers(this);
    }

    public PlayerControl getPlayerControl() {
        return playerControl;
    }

    public void setListener(ExoPlayer.Listener listener){
        this.player.addListener(listener);
    }

    public boolean getPlayWhenReady() {
        return this.player.getPlayWhenReady();
    }


//
//    private void maybeReportPlayerState() {
//        boolean playWhenReady = player.getPlayWhenReady();
//        int playbackState = getPlaybackState();
//        if (lastReportedPlayWhenReady != playWhenReady || lastReportedPlaybackState != playbackState) {
//            for (ExoPlayer.Listener listener : listeners) {
//                listener.onStateChanged(playWhenReady, playbackState);
//            }
//            lastReportedPlayWhenReady = playWhenReady;
//            lastReportedPlaybackState = playbackState;
//        }
//    }
//
//    public int getPlaybackState() {
//        if (rendererBuildingState == RENDERER_BUILDING_STATE_BUILDING) {
//            return STATE_PREPARING;
//        }
//        int playerState = player.getPlaybackState();
//        if (rendererBuildingState == RENDERER_BUILDING_STATE_BUILT && playerState == STATE_IDLE) {
//            // This is an edge case where the renderers are built, but are still being passed to the
//            // player's playback thread.
//            return STATE_PREPARING;
//        }
//        return playerState;
//    }

    //////////////////////////////////////////////////////CLASSES//////////////////////////////////



}
