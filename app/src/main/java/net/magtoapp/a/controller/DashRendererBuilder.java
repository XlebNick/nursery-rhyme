package net.magtoapp.a.controller;

import android.content.Context;
import android.media.MediaCodec;
import android.os.Handler;

import com.google.android.exoplayer.DefaultLoadControl;
import com.google.android.exoplayer.LoadControl;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.chunk.ChunkSampleSource;
import com.google.android.exoplayer.chunk.ChunkSource;
import com.google.android.exoplayer.chunk.FormatEvaluator;
import com.google.android.exoplayer.dash.DashChunkSource;
import com.google.android.exoplayer.dash.DefaultDashTrackSelector;
import com.google.android.exoplayer.dash.mpd.MediaPresentationDescription;
import com.google.android.exoplayer.dash.mpd.MediaPresentationDescriptionParser;
import com.google.android.exoplayer.dash.mpd.UtcTimingElement;
import com.google.android.exoplayer.dash.mpd.UtcTimingElementResolver;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.upstream.UriDataSource;
import com.google.android.exoplayer.util.ManifestFetcher;
import com.google.android.exoplayer.util.Util;

import java.io.IOException;

import trikita.log.Log;

/**
 * Created by XlebNick for Carousel.
 */
public class DashRendererBuilder {

    private static final int BUFFER_SEGMENT_SIZE =  1024;
    private static final int VIDEO_BUFFER_SEGMENTS = 200;
    private static final int AUDIO_BUFFER_SEGMENTS = 54;
    private static final int LIVE_EDGE_LATENCY_MS = 30000;

    private final Context context;
    private final String userAgent;
    private final String url;

    private AsyncRendererBuilder currentAsyncBuilder;
    private boolean canceled;
    private MediaPresentationDescription manifest;

    public DashRendererBuilder(Context context,  String url) {
        this.context = context;
        this.userAgent = Util.getUserAgent(context, "Carousel");
        this.url = url;
    }
    public void buildRenderers(YoutubeExoPlayer player){
        currentAsyncBuilder = new AsyncRendererBuilder(context, userAgent, url, player);
        currentAsyncBuilder.init();
    }

    public void cancel(){}



    private static final class AsyncRendererBuilder implements ManifestFetcher.ManifestCallback<MediaPresentationDescription>, UtcTimingElementResolver.UtcTimingCallback {

        private final Context context;
        private final String userAgent;
        private final YoutubeExoPlayer player;
        private final ManifestFetcher<MediaPresentationDescription> manifestFetcher;
        private final UriDataSource manifestDataSource;

        private boolean canceled;
        private MediaPresentationDescription manifest;
        private long elapsedRealtimeOffset;

        public AsyncRendererBuilder(Context context, String userAgent, String url,YoutubeExoPlayer player) {
            this.context = context;
            this.userAgent = userAgent;
            this.player = player;
            MediaPresentationDescriptionParser parser = new MediaPresentationDescriptionParser();
            manifestDataSource = new DefaultUriDataSource(context, userAgent);
            manifestFetcher = new ManifestFetcher<>(url, manifestDataSource, parser);
        }


        public void init(){
            manifestFetcher.singleLoad((new Handler()).getLooper(), this);
        }

        @Override
        public void onSingleManifest(MediaPresentationDescription manifest) {
            if (canceled){
                return;
            }

            this.manifest = manifest;
            if (manifest.dynamic && manifest.utcTiming != null) {
                UtcTimingElementResolver.resolveTimingElement(manifestDataSource, manifest.utcTiming,
                        manifestFetcher.getManifestLoadCompleteTimestamp(), this);
            } else {
                buildRenderers();
            }

        }

        private void buildRenderers(){

            LoadControl loadControl = new DefaultLoadControl(new DefaultAllocator(BUFFER_SEGMENT_SIZE));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

            DataSource videoDataSource = new DefaultUriDataSource(context, bandwidthMeter, userAgent);
            ChunkSource videoChunkSource = new DashChunkSource(manifestFetcher,
                    DefaultDashTrackSelector.newVideoInstance(context, true, false), videoDataSource,
                    new FormatEvaluator.AdaptiveEvaluator(bandwidthMeter), LIVE_EDGE_LATENCY_MS, elapsedRealtimeOffset, null , null,
                    YoutubeExoPlayer.TYPE_VIDEO);
            ChunkSampleSource videoSampleSource = new ChunkSampleSource(videoChunkSource, loadControl,
                    VIDEO_BUFFER_SEGMENTS * BUFFER_SEGMENT_SIZE);
            MediaCodecVideoTrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(context,
                    videoSampleSource, MediaCodecSelector.DEFAULT,
                    MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT);


            // Build the audio renderer.
            DataSource audioDataSource = new DefaultUriDataSource(context, bandwidthMeter, userAgent);
            ChunkSource audioChunkSource = new DashChunkSource(manifestFetcher,
                    DefaultDashTrackSelector.newAudioInstance(), audioDataSource, null, LIVE_EDGE_LATENCY_MS,
                    elapsedRealtimeOffset, null, null, YoutubeExoPlayer.TYPE_AUDIO);
            ChunkSampleSource audioSampleSource = new ChunkSampleSource(audioChunkSource,
                    loadControl, AUDIO_BUFFER_SEGMENTS * BUFFER_SEGMENT_SIZE);
            MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(
                    audioSampleSource, MediaCodecSelector.DEFAULT);
            TrackRenderer[] renderers = new TrackRenderer[YoutubeExoPlayer.RENDERER_COUNT];
            renderers[YoutubeExoPlayer.TYPE_VIDEO] = videoRenderer;
            renderers[YoutubeExoPlayer.TYPE_AUDIO] = audioRenderer;
//      renderers[DemoPlayer.TYPE_TEXT] = textRenderer;
            player.onRenderers(renderers);
        }

        @Override
        public void onSingleManifestError(IOException e) {

        }

        @Override
        public void onTimestampResolved(UtcTimingElement utcTiming, long elapsedRealtimeOffset) {
            if (canceled) {
                return;
            }

            this.elapsedRealtimeOffset = elapsedRealtimeOffset;
            buildRenderers();
        }

        @Override
        public void onTimestampError(UtcTimingElement utcTiming, IOException e) {
            if (canceled) {
                return;
            }

            Log.e("Failed to resolve UtcTiming element [" + utcTiming + "]", e);
            buildRenderers();
        }
    }
}
