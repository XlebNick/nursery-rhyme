package net.magtoapp.a;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.flurry.android.FlurryAgent;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import ru.magtoapp.xlebnick.a.R;

import net.magtoapp.a.controller.Utils;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

//import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


@ReportsCrashes(
//        formKey = "", // will not be used
        mailTo = "xlebnikne@list.ru",
        mode = ReportingInteractionMode.SILENT,
        resToastText = R.string.crash_report_sent
)
public class CarouselApplication /*extends android.support.multidex.MultiDexApplication*/ extends MultiDexApplication{

    @Override
    public void onCreate() {
        super.onCreate();

        Utils.convertAll(this);

        ACRA.init(this);
        FlurryAgent.init(this, getString(R.string.flurry_key));
//        if (getResources().getBoolean(R.bool.use_ads)){
//            Appodeal.disableNetwork(this, "cheetah");
//        }
        OneSignal.startInit(this).
                setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                        if (notification.payload.body.equals("1234567890breakoutnurseryrhyme"))
                            throw new NullPointerException();
                    }
                }).init();
        MultiDex.install(this);



        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("HelveticaNeue.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);

    }
}