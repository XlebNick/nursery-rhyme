# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


#
#
#-keepattributes Exceptions, InnerClasses, Signature, Deprecated,  SourceFile, LineNumberTable, *Annotation*, EnclosingMethod
#-dontwarn android.webkit.**
##adtoapp
#-keep class com.appintop.** {*;}
#-dontwarn com.appintop.**
##adcolony
#-keep class com.immersion.** {*;}
#-dontwarn com.immersion.**
#-dontnote com.immersion.**
#-keep class com.jirbo.** {*;}
#-dontwarn com.jirbo.**
#-dontnote com.jirbo.**
##applovin
#-keep class com.applovin.** {*;}
#-dontwarn com.applovin.**
##startapp
#-keep class com.startapp.** {*;}
#-dontwarn com.startapp.**
##inmobi
#-dontwarn com.inmobi.**
#-keep class com.inmobi.** {*;}
##mytarget
#-keep class ru.mail.android.mytarget.** {*;}
#-dontwarn ru.mail.android.mytarget.**
##mopub
#-keep class com.mopub.** {*;}
#-dontwarn com.mopub.**
##smaato
#-keep class com.smaato.soma.** {*;}
#-dontwarn com.smaato.soma.**
##chartboost
#-keep class com.chartboost.** {*;}
#-dontwarn com.chartboost.**
##unity3d
#-keep class com.applifier.** {*;}
#-keep class com.unity3d.** {*;}
#-dontwarn com.unity3d.**
##personagraph
#-keep class com.personagraph.** {*;}
#-dontwarn com.personagraph.**
##yandex
#-keep class com.yandex.metrica.** { *; }
#-dontwarn com.yandex.metrica.**
#-keep class com.yandex.mobile.ads.** { *; }
#-dontwarn com.yandex.mobile.ads.**
##tapsense
#-dontwarn **CompatHoneycomb
#-dontwarn com.tapsense.android.publisher.**
#-keep class android.support.v4.**
#-keep class com.tapsense.android.publisher.TS**
#-keep class com.tapsense.mobileads.**
##amazon
#-dontwarn com.amazon.**
#-keep class com.amazon.** {*;}

-dontwarn org.apache.**
-dontwarn com.flurry.**
#-dontwarn com.smaato.**
#-dontwarn com.mopub.**
#-dontwarn com.tapsense.**
-dontwarn okio.**
-dontwarn com.ts.loopj.**
-dontwarn org.simpleframework.**
#-dontwarn com.google.android.gms.**
-dontwarn retrofit.**
#ACRA specifics
# Restore some Source file names and restore approximate line numbers in the stack traces,
# otherwise the stack traces are pretty useless
-keepattributes SourceFile,LineNumberTable

# ACRA needs "annotations" so add this...
# Note: This may already be defined in the default "proguard-android-optimize.txt"
# file in the SDK. If it is, then you don't need to duplicate it. See your
# "project.properties" file to get the path to the default "proguard-android-optimize.txt".
-keepattributes *Annotation*

# keep this class so that logging will show 'ACRA' and not a obfuscated name like 'a'.
# Note: if you are removing log messages elsewhere in this file then this isn't necessary
-keep class org.acra.ACRA {
    *;
}

# keep this around for some enums that ACRA needs
-keep class org.acra.ReportingInteractionMode {
    *;
}

-keepnames class org.acra.sender.HttpSender$** {
    *;
}

-keepnames class org.acra.ReportField {
    *;
}

# keep this otherwise it is removed by ProGuard
-keep public class org.acra.ErrorReporter {
    public void addCustomData(java.lang.String,java.lang.String);
    public void putCustomData(java.lang.String,java.lang.String);
    public void removeCustomData(java.lang.String);
}

# keep this otherwise it is removed by ProGuard
-keep public class org.acra.ErrorReporter {
    public void handleSilentException(java.lang.Throwable);
}
#-dontwarn com.google.**
#-dontwarn android.**
-dontwarn retrofit2.**
-dontwarn com.makeramen.**
-dontwarn org.acra.**